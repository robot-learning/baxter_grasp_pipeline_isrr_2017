# Baxter Grasp Protocol

## Purpose
To experimentally assess the performance of the CNN Grasp network on Baxter. 

## Task Description
The following are the sequence of actions Baxter should perform for a Task to be successful

1. Move to the object.
2. Grasp the object.
3. Move the object 30 cms above the table.
4. Place the object in a new location.

## Setup Description

### List of objects 
We select objects that baxter can grasp in ideal conditions. This filters out objects that are too big to 
fit between baxter's grippers and objects that are too small like a knife.

Following is a list of objects used:

#### Wide grip

- pringles
- paint can
- screwdriver
- banana
- foam cube
- charger

#### thin grip

- blade
- cup
- clamp
- spatula

## Robot/Hardware Description
We use Baxter Robot fitted with parallel electric grippers.

## Proceduere
We will run the following steps for each object 5 times, varying the position and orientation of the object.

1. Place the object within baxters workspace
2. Use table segmentation to subtract the table and get object location
3. Sample 4 grasp initializations
4. Use these samples to predict a good grasp.
5. Choose the grasp with the best probability.
6. Execute this grasp
7. Move to the center of the object along the approach vector.
8. Pick the object up
9. Lift the object 30 cms above the table.
10. Place the object in a new location

