#include <ros/ros.h>
#include <tf/transform_listener.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

#include <sensor_msgs/PointCloud2.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>

#include <iostream>
#include "baxter_pipeline/GetNormals.h"

/*

~~Subscribe to a registered pointcloud topic~~

input pointcloud
compute normals for the pointcloud
send both normals and pointcloud as response

*/
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

image_transport::Publisher img_pub_;


bool find_normals(baxter_pipeline::GetNormals::Request &req,
                  baxter_pipeline::GetNormals::Response &res)
{
    // how to get the pointcloud from /camera/depth_registered/points
    // ros::Duration four_seconds(4.0);
    ros::NodeHandle nh;
    // ROS_INFO("Looking for camera feed...");

    // ROS_INFO("Found camera feed.");
    // ROS_INFO("Streaming data...");

    tf::TransformListener listener;

    bool can_transform;
    can_transform = listener.waitForTransform(req.input_cloud.header.frame_id,
                                                "base",
                                                ros::Time(0),
                                                ros::Duration(4.0));

    if (can_transform){
        ROS_INFO("Ready to transform cloud");
    } 

    req.input_cloud.header.stamp = ros::Time(0);
    pcl_ros::transformPointCloud("base", req.input_cloud,
                                    res.obj.transformed_cloud,
                                    listener);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr _cloud (new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::fromROSMsg(res.obj.transformed_cloud, *_cloud);


    ROS_INFO("Published Pointcloud. ");
    cv::Mat disp_img(_cloud->height, _cloud->width, CV_8UC3, cv::Scalar(0,0,0));
    ROS_INFO("(_cloud->height, width) = (%d, %d)", _cloud->height , _cloud->width);
    ROS_INFO("Created empty image");
    res.obj.p_cloud = req.input_cloud;
    ROS_INFO("Iterating through cloud");
    for(int r = 0;r < _cloud->height; ++r){
        for (int c = 0; c < _cloud->width; ++c){
            disp_img.at<cv::Vec3b>(r,c) = cv::Vec3b(_cloud->at(c,r).b, _cloud->at(c,r).g, _cloud->at(c,r).r);
        }
    }
    cv_bridge::CvImage img_bridge;
   
    std_msgs::Header header;
    header.stamp = ros::Time::now(); // time
    img_bridge = cv_bridge::CvImage(header, "bgr8", disp_img);
    //cv::CvImagePtr cv_ptr;

    img_bridge.toImageMsg(res.full_image);
    
    pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::Normal> ne;
    ne.setInputCloud(_cloud);

    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB> ());
    ne.setSearchMethod (tree);

    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    int normals_k_neighbors = 400;
    ne.setKSearch(normals_k_neighbors);

    ne.compute (*cloud_normals);

    ROS_INFO("Found Normals.");

    pcl::toROSMsg(*cloud_normals, res.obj.normals);

    ROS_INFO("Published Normals.");
    img_pub_.publish(img_bridge.toImageMsg());

    return true;

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "normal_calc");
    ros::NodeHandle n;// = nullptr;
    //ros::NodeHandle n;
    ros::ServiceServer service = n.advertiseService("get_normals", find_normals);
    image_transport::ImageTransport it_(n);
    img_pub_ = it_.advertise("/converted_cloud", 1);
    // listener    = new tf::TransformListener();

    ROS_INFO("Service Initialized.");
    ROS_INFO("Press Ctrl + C to stop running.");
    ros::spin();

    return 0;

}