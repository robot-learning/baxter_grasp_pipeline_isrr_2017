#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError

class CameraOverlay(object):
    """docstring for CameraOverlay"""
    def __init__(self):
        rospy.init_node('image_listener')

        self.bridge = CvBridge()
        rospy.Subscriber('/camera/rgb/image_raw', Image, self.image_callback)
        rospy.spin()

    def image_callback(self, msg):
        # print('Received an image!')
        try:
            # Convert your ROS Image message to OpenCV2
            cv2_img = self.bridge.imgmsg_to_cv2(msg, 'bgr8')
            old_img = cv2.imread('/home/hashb/tmp/plots/init0/rgb.png')
            alpha = 0.3
            cv2.addWeighted(old_img, alpha, cv2_img, 1-alpha, 0, cv2_img)
            cv2.imshow('ghost_image', cv2_img)
            cv2.waitKey(3)
        except CvBridgeError, e:
            print(e)
        rospy.sleep(0.1)

a = CameraOverlay()
# rospy.spin()