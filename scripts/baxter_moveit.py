#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import baxter_interface
from baxter_pipeline.srv import *
from moveit_commander import RobotCommander, PlanningSceneInterface, roscpp_initialize, roscpp_shutdown

from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
import tf
import numpy as np

# from grasp_inf import inference as inf

_DEBUG = False
_SEP = "\#"*15, '\n'

class GraspExecutionNode(object):
    """docstring for GraspInfNode"""
    def __init__(self):
        rospy.init_node("moveit_py")
        
        self.right_gripper = baxter_interface.Gripper('right')
        self.right_gripper.calibrate()

        self.left_gripper = baxter_interface.Gripper('left')
        self.left_gripper.calibrate()

        self.tf = tf.TransformListener()
        
        # self.baxter = baxter_interface.RobotEnable()
        self.jts_right = ['right_e0', 'right_e1', 'right_s0', 'right_s1', 'right_w0', 'right_w1', 'right_w2']
        self.jts_left = ['left_e0', 'left_e1', 'left_s0', 'left_s1', 'left_w0', 'left_w1', 'left_w2']

        self.lpos1 = [-1.441426162661994, 0.8389151064712133, 0.14240920034028015, -0.14501001475655606, -1.7630090377446503, -1.5706376573674472, 0.09225918246029519]
        self.rpos1 = [1.7238109084167481, 1.7169079948791506, 0.36930587426147465, -0.33249033539428713, -1.2160632682067871, 1.668587600115967, -1.810097327636719]

        self.scene = moveit_commander.PlanningSceneInterface()

        self.right_group = moveit_commander.MoveGroupCommander('right_arm')
        self.left_group = moveit_commander.MoveGroupCommander('left_arm')

        # print self.get_transform('/base', '/ar')
        self.table_pose = self.get_posestamped_from_tuple(self.get_transform('/base', '/ar'))
        self.table_pose.pose.position.x =  1.28
        self.table_pose.pose.position.y =  0.03
        self.table_pose.pose.position.z = -0.53


        camera_pose = self.get_transform('/base', '/camera_link')
        camera_pose = ((0.17, 0.03, 0.57), camera_pose[1])

        self.camera_pose = self.get_posestamped_from_tuple(camera_pose)

        self.scene.add_box('table', self.table_pose, (1.524, 1.524, 0.762))
        self.scene.add_box('xtion', self.camera_pose, (0.0508, 0.1778, 0.0254))
        self.left_group.set_planner_id('RRTStarkConfigDefault')
        self.right_group.set_planner_id('RRTStarkConfigDefault')

        self.robot = moveit_commander.RobotCommander()


    def get_posestamped_from_tuple(self, pose_tuple):
        r = PoseStamped()
        r.header.stamp = rospy.Time()
        r.header.frame_id = 'base'
        r.pose = Pose(Point(*pose_tuple[0]), Quaternion(*pose_tuple[1]))
        return r

    def get_transform(self, from_frame, to_frame):
        self.tf.waitForTransform(from_frame, to_frame, rospy.Time(), rospy.Duration(4.0))
        if self.tf.frameExists(from_frame):
            if self.tf.frameExists(to_frame):
                latest_time = self.tf.getLatestCommonTime(from_frame, to_frame)
                position, quaternion = self.tf.lookupTransform(from_frame, to_frame, latest_time)
                return (position, quaternion)
            else:
                rospy.logerr("Cannot find {} frame".format(to_frame))
                raise ValueError('check if your cameras are looking at the marker and your robot tf is loaded') 
        else:
            rospy.logerr("Cannot find {} frame".format(from_frame))
            raise ValueError('check if your cameras are looking at the marker and your robot tf is loaded') 

    def close_gripper(self):
        self.left_gripper.close()

    def open_gripper(self):
        self.left_gripper.open()

    def move_to_object(self):
        pass

    def publish_gripper_goal_pose(self, transformed_goal_pose):
        br = tf.TransformBroadcaster()
        while not rospy.is_shutdown():
            br.sendTransform((transformed_goal_pose.pose.position.x, transformed_goal_pose.pose.position.y, 
                              transformed_goal_pose.pose.position.z),
                             (transformed_goal_pose.pose.orientation.x, transformed_goal_pose.pose.orientation.y,
                             transformed_goal_pose.pose.orientation.z, transformed_goal_pose.pose.orientation.w),
                             rospy.Time.now(),
                             'gripper_pose',
                             transformed_goal_pose.header.frame_id)
            rospy.sleep(5)

    def run(self):
        
        if _DEBUG:
            print _SEP
            print 'Planning Frame: ', self.group.get_planning_frame()
            print _SEP
            print 'End-effector Link: ', self.group.get_end_effector_link()
            print _SEP
            print 'Robot Groups: ', self.robot.get_group_names()
            print _SEP
            print 'Robot State: ', self.robot.get_current_state()
            print """DEBUG: 
                        If robot states are all zeros, then try mapping 
                        /robot/joint_states to /joint_states using
                        'rosrun topic_tools relay /robot/joint_states /joint_states'"""
            print _SEP

        grasp_inference_proxy = rospy.ServiceProxy('grasp_inf', GripperGraspInf)
        grasp_req = GripperGraspInfRequest()
        grasp_response = grasp_inference_proxy(grasp_req)
        transformed_goal_pose = self.tf.transformPose('/base', grasp_response.gripper_pose)
        # print grasp_response.normal
        gripper_normal = np.array(grasp_response.normal)
        print "transformed goal pose"
        print transformed_goal_pose
        print ""

        p = PoseStamped()
        p.header.frame_id = self.robot.get_planning_frame()
        p.pose.position = transformed_goal_pose.pose.position
        self.scene.add_box("part", p, (0.08, 0.09, 0.08))

        # self.robot.left_arm.pick("part")

        # rospy.spin()
        # roscpp_shutdown()

        self.group = self.left_group
        self.group.set_pose_target(transformed_goal_pose)
        # print goal_pose

        self.group.set_planning_time(15)
        self.group.set_num_planning_attempts(5)

        plan = self.group.plan()

        if plan.joint_trajectory.header.frame_id:
            self.open_gripper()

            self.group.execute(plan)
            
            rospy.sleep(5)
            self.close_gripper() # TODO: set gripper close param based on object

            if raw_input("should i move forward? ") == "y":
                new_goal = Pose()
                new_goal.position.x = 0.648
                new_goal.position.y = 0.056
                new_goal.position.z = 0.260

                new_goal.orientation.w = -0.383
                new_goal.orientation.x = 0.918
                new_goal.orientation.y = -0.083
                new_goal.orientation.z = -0.064

                # transformed_new_goal_pose = self.tf.transformPose('/base', new_goal)
                self.group.set_pose_target(new_goal)
                
                plan = self.group.plan()
                
                if plan.joint_trajectory.header.frame_id:
                    self.group.execute(plan)
                    rospy.sleep(5)


        else:
            print 'Failed to find a Plan for the goal pose with left arm'
            # print 'Trying with right arm'

            # self.right_group.set_pose_target(transformed_goal_pose)
            # plan = self.right_group.plan()
            # if plan.joint_trajectory.header.frame_id:
            #     self.right_group.execute(plan)


        run_again = raw_input('should I run it again [y/n] > ')
        if run_again == 'y':
            self.run()
        else:
            self.publish_gripper_goal_pose(transformed_goal_pose)
            print "I'll be running idle till you press Ctrl+C"



if __name__ == '__main__':
    grasping = GraspExecutionNode()
    grasping.run()
    print "exited grasping.run()"
    # rospy.spin()