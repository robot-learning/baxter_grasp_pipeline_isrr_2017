#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import baxter_interface
from moveit_python import PlanningSceneInterface, MoveGroupInterface
from baxter_pipeline.srv import *

import os
from geometry_msgs.msg import PoseStamped, Pose
import tf


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('baxter_moveit_testing',
                anonymous=True)
tl = tf.TransformListener()

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("left_arm")

# zero_pose = geometry_msgs.msg.Pose()
# zero_pose.position.x = 0.6143
# zero_pose.position.y = 0.5327
# zero_pose.position.z = 0.8793
# zero_pose.orientation.x = 0.8245
# zero_pose.orientation.y = 0.2759
# zero_pose.orientation.z = 0.4939
# zero_pose.orientation.w = 0.0020

# group.set_pose_target(zero_pose)
# zero_plan = group.plan()
# group.execute(zero_plan)

grasp_inference_proxy = rospy.ServiceProxy('grasp_inf', GripperGraspInf)
grasp_req = GripperGraspInfRequest()
goal_pose = grasp_inference_proxy(grasp_req)

transformed_goal_pose = tl.transformPose('base', goal_pose.gripper_pose)

print "Transformation pose from base",transformed_goal_pose.pose
g = transformed_goal_pose.pose

# Known Pose
# g = Pose()
# # 0.823, 0.113, -0.196
# # 0.081, 0.992, 0.006, 0.097
# g.position.x = 0.823
# g.position.y = 0.113
# g.position.z = -0.196
# g.orientation.x = 0.081
# g.orientation.y = 0.992
# g.orientation.z = 0.006
# g.orientation.w = 0.097

# group.set_planner_id('RRTConnectkConfigDefault')
group.set_planner_id('RRTStarkConfigDefault')
group.set_pose_target(g)
group.set_planning_time(15)
plan1 = group.plan()


if "y" == raw_input("do you want me to execute it> "):
	group.execute(plan1)
	
# moveit_commander.roscpp_shutdown()
br = tf.TransformBroadcaster()
while not rospy.is_shutdown():
    br.sendTransform((transformed_goal_pose.pose.position.x, transformed_goal_pose.pose.position.y, 
                      transformed_goal_pose.pose.position.z),
                     (transformed_goal_pose.pose.orientation.x, transformed_goal_pose.pose.orientation.y,
                     transformed_goal_pose.pose.orientation.z, transformed_goal_pose.pose.orientation.w),
                     rospy.Time.now(),
                     'gripper_pose',
                     transformed_goal_pose.header.frame_id)
    rospy.sleep(5)