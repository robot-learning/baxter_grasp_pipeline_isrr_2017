import cv2
import numpy as np
import time
import os


def main():
    pcd_nums = [100] * 10
    pcd_nums[8] = 50
    pcd_nums[9] = 35
    data_path = '/media/kai/cornell_grasp_data/'
    total_pcd_num = np.sum(pcd_nums)
    test_pcd_num = int(0.9 * total_pcd_num)

    start_time = time.time()
    prev_num = 0
    #cur_grasp_num = 1
    #correct_pred_num = 0
    #grasp_labels = []
    #pred_labels = []
    pre_path = '/media/kai/tmp/'
    for i, n in enumerate(pcd_nums):
        if i >= 1:
            prev_num += pcd_nums[i - 1]
        # if i < 9:
        #    continue
        # if i > 0 and i < 9:
        #    continue
        for j in xrange(n):
            rgbd_id = prev_num + j
            if rgbd_id < test_pcd_num:
                continue
            print 'rgbd_id:', rgbd_id
            print i, j

            path_img = pre_path + 'images/' + \
                str(rgbd_id) + '_' + str(20) + '/rgbd/rgb.png'
            dst_path_img = pre_path + 'images_copy/' + \
                str(rgbd_id) + '_img.png'
            cmd_1 = 'cp ' + path_img + ' ' + dst_path_img
            print cmd_1
            os.system(cmd_1)
            path_img_opt = pre_path + 'images_opt/' + \
                str(rgbd_id) + '_' + str(20) + '/rgbd/rgb.png'
            dst_path_img_opt = pre_path + \
                'images_copy/' + str(rgbd_id) + '_opt.png'
            cmd_2 = 'cp ' + path_img_opt + ' ' + dst_path_img_opt
            print cmd_2
            os.system(cmd_2)

            elapsed_time = time.time() - start_time
            print 'elapsed_time: ', elapsed_time
            print '####################################'

    elapsed_time = time.time() - start_time
    print 'total elapsed_time: ', elapsed_time


if __name__ == '__main__':
    main()
