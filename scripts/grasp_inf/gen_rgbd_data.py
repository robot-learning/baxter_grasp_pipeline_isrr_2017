import numpy as np
import time
import h5py
import cv2

img_rows = 480
img_cols = 640
channels = 8
rgb_channels = 3
depth_channels = 5


def extract_depth_info(pcd_path, normal_path):
    depth_info = np.zeros((img_rows, img_cols, depth_channels))
    pcd_file = open(pcd_path, 'r')
    pcd_lines = pcd_file.readlines()
    #pcd_lines_split = pcd_lines[10:].split()
    pcd_lines_split = np.array(map(str.split, pcd_lines[10:]))
    # print pcd_lines_split
    # print type(pcd_lines_split), type(pcd_lines_split[0])
    # print pcd_lines_split[0:2, 2]
    depth = np.array(map(float, pcd_lines_split[:, 2]))
    #depth /= 1000.
    # print 'depth:', depth[0], depth[-1]
    # print 'max, min depth:', np.max(depth), np.min(depth)
    index = np.array(map(int, pcd_lines_split[:, -1]))
    # print 'index:', index[0], index[-1]
    index_row = index / img_cols
    index_col = index % img_cols
    depth_info[index_row, index_col, 0] = depth
    pcd_file.close()

    normal_file = open(normal_path, 'r')
    normal_lines = normal_file.readlines()
    #normal_lines_split = normal_lines[11:].split()
    normal_lines_split = np.array(map(str.split, normal_lines[11:]))
    # print '# of points', len(normal_lines_split)
    # if len(normal_lines_split) != len(pcd_lines_split):
    #     print 'Different number of normals than points!'
    #     return

    normals = np.array(map(lambda x: map(float, x), normal_lines_split))
    print 'normals: ', normals[0, :], normals[-1, :]
    depth_info[index_row, index_col, 1:] = normals
    normal_file.close()

    print 'depth_info:', depth_info[0, 0, :], depth_info[240, 320, :]
    return depth_info

rgbd_file = h5py.File(
    '/media/kai/tars_HDD/cornell_grasp_data/h5_data/rgbd.h5', 'w')

pcd_nums = [100] * 10
pcd_nums[8] = 50
pcd_nums[9] = 35
data_path = '/media/kai/tars_HDD/cornell_grasp_data/'
total_pcd_num = np.sum(pcd_nums)
rgbd_data = np.zeros((total_pcd_num, img_rows, img_cols, channels))

start_time = time.time()
prev_num = 0
for i, n in enumerate(pcd_nums):
    #prev_num = np.sum(pcd_nums[:i+1])
    if i >= 1:
        prev_num += pcd_nums[i - 1]
    for j in xrange(n):
        img_file = data_path + str(i + 1).zfill(2) + '/pcd' + \
            str(i + 1).zfill(2) + str(j).zfill(2) + 'r.png'
        pcd_file = data_path + str(i + 1).zfill(2) + '/pcd' + \
            str(i + 1).zfill(2) + str(j).zfill(2) + '.txt'
        normal_file = data_path + str(i + 1).zfill(2) + '/pcd' + \
            str(i + 1).zfill(2) + str(j).zfill(2) + 'normal.txt'
        print pcd_file
        img = cv2.imread(img_file, cv2.IMREAD_COLOR)
        rgbd_data[prev_num + j, :, :, :rgb_channels] = img
        rgbd_data[prev_num + j, :, :,
                  rgb_channels:] = extract_depth_info(pcd_file, normal_file)
        print 'rgb:', img[0, 0, :], img[240, 320, :]
        print 'rgbd_data:', rgbd_data[prev_num + j, 0, 0, :], rgbd_data[prev_num + j, 240, 320, :]

        elapsed_time = time.time() - start_time
        print 'elapsed_time: ', elapsed_time
        print '####################################'

rgbd_file.create_dataset('RGBD', data=rgbd_data)
elapsed_time = time.time() - start_time
print 'total elapsed_time: ', elapsed_time

rgbd_file.close()
