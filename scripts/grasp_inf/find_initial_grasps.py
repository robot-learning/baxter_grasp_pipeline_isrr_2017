import cv2
import numpy as np
import os
import time


def read_bg_mapping():
    bg_map_path = '/media/kai/cornell_grasp_data/backgroundMapping.txt'
    bg_map_file = open(bg_map_path, 'r')
    bg_map_lines = bg_map_file.readlines()
    bg_map_lines_split = np.array(map(str.split, bg_map_lines))
    # print bg_map_lines_split
    bg_map = {}
    # map(lambda x: bg_img[x[0]] = x[1], bg_map_lines_split)
    for line in bg_map_lines_split:
        bg_map[line[0]] = line[1]
    # print bg_map
    bg_map_file.close()
    return bg_map


def find_obj_ellipse(fg, bg):
    # fg and bg are paths to images
    data_path = '/home/hashb/tmp/'
    # img_file = 'pcd' + 'r.png'
    # bg_file = bg_map[img_file]
    # bg_file = bg_file[:-10] + bg_file[-9:]
    # img_path = data_path + '/' + img_file
    # bg_img_path = data_path + 'backgrounds/' + bg_file
    # print img_path, bg_img_path

    bg_img = cv2.imread(bg, cv2.IMREAD_GRAYSCALE)
    img = cv2.imread(fg, cv2.IMREAD_GRAYSCALE)

    fg_img = img - bg_img
    lower_gray_value = 20
    upper_gray_value = 220
    fg_img[(fg_img < lower_gray_value)] = 0
    fg_img[(fg_img > upper_gray_value)] = 0

    img_rows = 480
    img_cols = 640
    # Cut off the boundary.
    fg_img[:img_rows / 3, :] = 0
    fg_img[img_rows * 3 / 4:, :] = 0
    fg_img[:, :img_cols / 4] = 0
    fg_img[:, img_cols * 3 / 4:] = 0
    fg_img_path = data_path + 'foregrounds/' + '_'
    if not os.path.exists(fg_img_path):
        os.makedirs(fg_img_path)
    ellipse_file_path = fg_img_path + '/ellipse_' + '.txt'
    # ellipse_img_path = fg_img_path + '/ellipse_' + \
    #     str(i + 1).zfill(2) + str(j).zfill(2) + '.png'
    # fg_img_path = fg_img_path + '/fg_' + \
    #     str(i + 1).zfill(2) + str(j).zfill(2) + '.png'
    # cv2.imwrite(fg_img_path, fg_img)

    ret, thresh = cv2.threshold(fg_img, lower_gray_value + 20, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, 1, 2)
    max_cnt_id = np.argmax([len(x) for x in contours])
    if len(contours[max_cnt_id]) < 50:
        ret, thresh = cv2.threshold(fg_img, lower_gray_value, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, 1, 2)
        max_cnt_id = np.argmax([len(x) for x in contours])
    # print np.array(contours).shape
    # print np.array(contours[max_cnt_id]).shape, contours[max_cnt_id]
    ellipse = cv2.fitEllipse(contours[max_cnt_id])
    # print ellipse
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.drawContours(img, contours, -1, (255, 0, 0), 3)
    cv2.ellipse(img, ellipse, (0, 255, 0), 2)
    #rect = cv2.minAreaRect(cnt)
    box = cv2.cv.BoxPoints(ellipse)
    # print box
    #box = np.int0(box)
    cv2.drawContours(img, [np.int0(box)], 0, (0, 0, 255), 2)
    # cv2.imwrite(ellipse_img_path, img)
    (x, y), (MA, ma), angle = ellipse
    ellipse_file = open(ellipse_file_path, 'w')
    ellipse_file.writelines(str(x) + ' ' + str(y) + ' ' +
                            str(MA) + ' ' + str(ma) + ' ' + str(angle))
    for i in xrange(4):
        ellipse_file.writelines('\n' + str(box[i][0]) + ' ' + str(box[i][1]))
    ellipse_file.close()

    f = open(ellipse_file_path, 'r')
    f.read()
    tmp_data = f
    f.close()

    return tmp_data


def fit_ellipse_pca(data):
    # Step 1: center the data to get the translation vector.
    center = data.mean(axis=1)
    # print 'Translation', center
    data -= np.reshape(center, (2, 1))

    # Step 2: perform PCA to find rotation matrix.
    scatter = np.dot(data, data.T)
    eigenvalues, transform = np.linalg.eig(scatter)
    if eigenvalues[0] < eigenvalues[1]:
        eigenvalues = eigenvalues[::-1]
        transform = transform[:, ::-1]
    # print 'eigenvalues:', eigenvalues
    # print 'Rotation matrix', transform

    # Step 3: Rotate back the data and compute radii.
    # You can also get the radii from smaller to bigger
    # with 2 / np.sqrt(eigenvalues)
    rotated = np.dot(np.linalg.inv(transform), data)
    radii = 2 * rotated.std(axis=1)
    # print 'Radii', radii

    angle = np.degrees(np.arctan2(transform[1, 1], transform[0, 1]))
    # Positive angle is counter cloeck wise in python, which is opposite
    # in opencv.
    angle = (angle + 180) % 360
    #angle %= 360

    return ((center[0], center[1]), (radii[1], radii[0]), angle)


def find_obj_ellipse_pca(fg, bg):
    # Note: requires foreground and background images in png format
    data_path = '/home/hashb/tmp/'
    # img_file = 'pcd' + 'r.png'
    # bg_file = bg_map[img_file]
    # bg_file = bg_file[:-10] + bg_file[-9:]
    # img_path = data_path + str(i + 1).zfill(2) + '/' + img_file
    # bg_img_path = data_path + 'backgrounds/' + bg_file
    # print img_path, bg_img_path

    bg_img = cv2.imread(bg, cv2.IMREAD_GRAYSCALE)
    img = cv2.imread(fg, cv2.IMREAD_GRAYSCALE)

    fg_img = img - bg_img
    lower_gray_value = 20
    upper_gray_value = 220
    fg_img[(fg_img < lower_gray_value)] = 0
    fg_img[(fg_img > upper_gray_value)] = 0

    img_rows = 480
    img_cols = 640
    # Cut off the boundary.
    fg_img[:img_rows / 3, :] = 0
    fg_img[img_rows * 3 / 4:, :] = 0
    fg_img[:, :img_cols / 4] = 0
    fg_img[:, img_cols * 3 / 4:] = 0
    fg_img_path = data_path + 'foregrounds/' + '_'
    if not os.path.exists(fg_img_path):
        os.makedirs(fg_img_path)
    ellipse_file_path = fg_img_path + '/ellipse_' + '.txt'
    ellipse_img_path = fg_img_path + '/ellipse_' + '.png'
    # ellipse_img_path = fg_img_path + '/ellipse_' + \
    #     str(i + 1).zfill(2) + str(j).zfill(2) + '.png'
    # fg_img_path = fg_img_path + '/fg_' + \
    #     str(i + 1).zfill(2) + str(j).zfill(2) + '.png'
    # cv2.imwrite(fg_img_path, fg_img)

    ret, thresh = cv2.threshold(fg_img, lower_gray_value + 20, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, 1, 2)
    max_cnt_id = np.argmax([len(x) for x in contours])
    if len(contours[max_cnt_id]) < 50:
        ret, thresh = cv2.threshold(fg_img, lower_gray_value, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, 1, 2)
        max_cnt_id = np.argmax([len(x) for x in contours])
    # print np.array(contours).shape
    # print np.array(contours[max_cnt_id]).shape, contours[max_cnt_id]
    cnt_points = np.array(contours[max_cnt_id])[:, 0, :].T
    # print cnt_points
    ellipse = fit_ellipse_pca(cnt_points.astype(float))
    # print ellipse
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.drawContours(img, contours, -1, (255, 0, 0), 3)
    cv2.ellipse(img, ellipse, (0, 255, 0), 2)
    #rect = cv2.minAreaRect(cnt)
    box = cv2.cv.BoxPoints(ellipse)
    # print box
    #box = np.int0(box)
    cv2.drawContours(img, [np.int0(box)], 0, (0, 0, 255), 2)
    cv2.imwrite(ellipse_img_path, img)
    (x, y), (MA, ma), angle = ellipse
    ellipse_file = open(ellipse_file_path, 'w')
    ellipse_file.writelines(str(x) + ' ' + str(y) + ' ' +
                            str(MA) + ' ' + str(ma) + ' ' + str(angle))
    for i in xrange(4):
        ellipse_file.writelines('\n' + str(box[i][0]) + ' ' + str(box[i][1]))
    ellipse_file.close()

    f = open(ellipse_file_path, 'r')
    tmp_data = f.readlines()
    f.close()

    return tmp_data


def main():
    pcd_nums = [100] * 10
    pcd_nums[8] = 50
    pcd_nums[9] = 35
    data_path = '/media/kai/tars_HDD/cornell_grasp_data/'
    total_pcd_num = np.sum(pcd_nums)

    bg_map = read_bg_mapping()
    start_time = time.time()
    for i, n in enumerate(pcd_nums):
        for j in xrange(n):
            #find_obj_ellipse(i, j, bg_map)
            find_obj_ellipse_pca(i, j, bg_map)
            # break
    elapsed_time = time.time() - start_time
    print 'total elapsed_time: ', elapsed_time

if __name__ == '__main__':
    # main()
    pass
