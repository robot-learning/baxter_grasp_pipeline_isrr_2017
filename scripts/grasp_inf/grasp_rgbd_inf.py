import os
import time
import cv2
import numpy as np
from grasp_rgbd_net import GraspRgbdNet
import gen_rgbd_patches as gp
import matplotlib.pyplot as plt


class GraspRgbdInf:

    def __init__(self):
        self.grasp_rgbd_net = GraspRgbdNet()
        self.grasp_rgbd_net.init_net_inf()
        # rgbd dimension: 480*600
        # 500*0.005=2.5
        self.x_eps = 2
        self.y_eps = 2
        self.w_eps = 2
        # 2pi*0.005
        self.theta_eps = np.pi * 0.01
        self.rgbd_rows = 480
        self.rgbd_cols = 640
        self.max_grasp_width = 60
        self.min_grasp_width = 10

    def get_pred_rect(self, rgbd, rect):
        palm_patch, finger_1_patch, finger_2_patch = \
            gp.extract_rgbd_patches(rgbd, rect)
        d_prob_d_palm, d_prob_d_f_1, d_prob_d_f_2, suc_prob = \
            self.grasp_rgbd_net.get_rgbd_gradients(
                palm_patch, finger_1_patch, finger_2_patch)
        # print d_prob_d_palm, d_prob_d_f_1, d_prob_d_f_2
        # print np.mean(np.abs(d_prob_d_palm)), np.mean(np.abs(d_prob_d_f_1)),
        # np.mean(np.abs(d_prob_d_f_2))
        positive = 1
        if suc_prob < 0.5:
            positive = 0
        return positive

    def get_pred(self, rgbd, q):
        palm_patch, finger_1_patch, finger_2_patch = \
            gp.extract_rgbd_patches_cfg(rgbd, q)
        d_prob_d_palm, d_prob_d_f_1, d_prob_d_f_2, suc_prob = \
            self.grasp_rgbd_net.get_rgbd_gradients(
                palm_patch, finger_1_patch, finger_2_patch)
        # print np.mean(np.abs(d_prob_d_palm)), np.mean(np.abs(d_prob_d_f_1)), np.mean(np.abs(d_prob_d_f_2))
        #positive = 1
        # if suc_prob < 0.5:
        #    positive = 0
        # return positive
        return suc_prob

    def transform_grad_img(self, grad_img, gray, normalize=False, scale_num=100):
        gray_grad_img = None
        if not gray:
            gray_grad_img = np.mean(grad_img, axis=2)
        else:
            gray_grad_img = grad_img
        if normalize:
            gray_grad_img = (gray_grad_img - np.min(gray_grad_img)) / \
                (np.max(gray_grad_img) - np.min(gray_grad_img))
        else:
            gray_grad_img *= scale_num
            gray_grad_img += 0.5
        return gray_grad_img * 255.0

    def binary_grad_img(self, grad_img, gray):
        gray_grad_img = None
        if not gray:
            gray_grad_img = np.mean(grad_img, axis=2)
        else:
            gray_grad_img = grad_img
        gray_grad_img = gray_grad_img > 0
        return gray_grad_img * 255.0

    # finger_num: 0 represents the palm.
    def plot_rgbd_grad(self, rgbd_grad, rgbd_id, iter_num,
                       grad_name, binary):
        grad_proc = np.copy(rgbd_grad[:, :, :4])
        if binary:
            grad_proc[:, :, 0] = self.binary_grad_img(
                rgbd_grad[:, :, :3], False)
            grad_proc[:, :, 1] = self.binary_grad_img(rgbd_grad[:, :, 3], True)
            grad_proc[:, :, 2] = self.binary_grad_img(
                rgbd_grad[:, :, 4:7], False)
            grad_proc[:, :, 3] = self.binary_grad_img(rgbd_grad[:, :, 7], True)
        else:
            grad_proc[:, :, 0] = self.transform_grad_img(
                rgbd_grad[:, :, :3], False, False)
            grad_proc[:, :, 1] = self.transform_grad_img(
                rgbd_grad[:, :, 3], True, False)
            grad_proc[:, :, 2] = self.transform_grad_img(
                rgbd_grad[:, :, 4:7], False, False)
            grad_proc[:, :, 3] = self.transform_grad_img(
                rgbd_grad[:, :, 7], True, False)

        # path = '/media/kai/tmp/gradients/rgbd_' + str(rgbd_id) + '/'
        # if not os.path.exists(path):
        #     os.makedirs(path)
        # path += 'iter_' + str(iter_num) + '/'
        # if not os.path.exists(path):
        #     os.makedirs(path)

        # # if finger_num == 0:
        # #    path += 'palm_'
        # # else:
        # #    path += 'finger' + str(finger_num) + '_'
        # path += grad_name + '_'

        # if binary:
        #     cv2.imwrite(path + 'rgb_bi.png', grad_proc[:, :, 0])
        #     cv2.imwrite(path + 'depth_bi.png', grad_proc[:, :, 1])
        #     cv2.imwrite(path + 'normal_bi.png', grad_proc[:, :, 2])
        #     cv2.imwrite(path + 'curv_bi.png', grad_proc[:, :, 3])
        # else:
        #     cv2.imwrite(path + 'rgb.png', grad_proc[:, :, 0])
        #     cv2.imwrite(path + 'depth.png', grad_proc[:, :, 1])
        #     cv2.imwrite(path + 'normal.png', grad_proc[:, :, 2])
        #     cv2.imwrite(path + 'curv.png', grad_proc[:, :, 3])

    def plot_rgbd_grad_both(self, rgbd_grad, rgbd_id, iter_num,
                            grad_name):
        self.plot_rgbd_grad(rgbd_grad, rgbd_id, iter_num,
                            grad_name, binary=False)
        self.plot_rgbd_grad(rgbd_grad, rgbd_id, iter_num,
                            grad_name, binary=True)

    def compute_grad(self, rgbd, q, save_grad=False, rgbd_id=None,
                     iter_num=None):
        palm_patch, finger_1_patch, finger_2_patch = \
            gp.extract_rgbd_patches_cfg(rgbd, q)
        d_prob_d_palm, d_prob_d_f_1, d_prob_d_f_2, suc_prob = \
            self.grasp_rgbd_net.get_rgbd_gradients(
                palm_patch, finger_1_patch, finger_2_patch)
        #positive = False
        # if suc_prob > 0.5:
        #    positive = True
        # return positive
        # print d_prob_d_palm, d_prob_d_f_1, d_prob_d_f_2
        # print 'patches mean:', np.mean(palm_patch),\
        #     np.mean(finger_1_patch), np.mean(finger_2_patch)
        # print 'patches std:', np.std(palm_patch),\
        #     np.std(finger_1_patch), np.std(finger_2_patch)
        # #cv2.imwrite('/media/kai/tmp/palm.png', np.array(palm_patch[:,:,:3]).astype(int))
        # #cv2.imwrite('/media/kai/tmp/f_1.png', np.array(finger_1_patch[:,:,:3]).astype(int))
        # #cv2.imwrite('/media/kai/tmp/f_2.png', np.array(finger_2_patch[:,:,:3]).astype(int))
        # print 'BP gradients sum:', np.sum(d_prob_d_palm),\
        #     np.sum(d_prob_d_f_1), np.sum(d_prob_d_f_2)
        # print 'BP gradients std:', np.std(d_prob_d_palm),\
        #     np.std(d_prob_d_f_1), np.std(d_prob_d_f_2)

        # print d_prob_d_palm.shape, d_prob_d_f_1.shape, d_prob_d_f_2.shape

        # Gradients of patches with respect to the x coordinate of the palm
        # center
        x_plus_eps = q + np.array([self.x_eps, .0, .0, .0])
        palm_patch_x_plus, f_1_patch_x_plus, f_2_patch_x_plus = \
            gp.extract_rgbd_patches_cfg(rgbd, x_plus_eps)
        x_minus_eps = q + np.array([-self.x_eps, .0, .0, .0])
        palm_patch_x_minus, f_1_patch_x_minus, f_2_patch_x_minus = \
            gp.extract_rgbd_patches_cfg(rgbd, x_minus_eps)
        d_patch_palm_d_x = (np.array(palm_patch_x_plus) -
                            np.array(palm_patch_x_minus)) / (2 * self.x_eps)
        d_patch_f_1_d_x = (np.array(f_1_patch_x_plus) -
                           np.array(f_1_patch_x_minus)) / (2 * self.x_eps)
        d_patch_f_2_d_x = (np.array(f_2_patch_x_plus) -
                           np.array(f_2_patch_x_minus)) / (2 * self.x_eps)
        # print np.array(palm_patch_x_plus), np.array(palm_patch_x_plus).shape
        # print d_patch_f_1_d_x.shape

        # Gradients of patches with respect to the y coordinate of the palm
        # center
        y_plus_eps = q + np.array([0., self.y_eps, .0, .0])
        palm_patch_y_plus, f_1_patch_y_plus, f_2_patch_y_plus = \
            gp.extract_rgbd_patches_cfg(rgbd, y_plus_eps)
        y_minus_eps = q + np.array([0., -self.y_eps, .0, .0])
        palm_patch_y_minus, f_1_patch_y_minus, f_2_patch_y_minus = \
            gp.extract_rgbd_patches_cfg(rgbd, y_minus_eps)
        d_patch_palm_d_y = (np.array(palm_patch_y_plus) -
                            np.array(palm_patch_y_minus)) / (2 * self.y_eps)
        d_patch_f_1_d_y = (np.array(f_1_patch_y_plus) -
                           np.array(f_1_patch_y_minus)) / (2 * self.y_eps)
        d_patch_f_2_d_y = (np.array(f_2_patch_y_plus) -
                           np.array(f_2_patch_y_minus)) / (2 * self.y_eps)

        # Gradients of patches with respect to the angle of the gripper plate
        theta_plus_eps = q + np.array([0., 0., self.theta_eps, .0])
        palm_patch_theta_plus, f_1_patch_theta_plus, f_2_patch_theta_plus = \
            gp.extract_rgbd_patches_cfg(rgbd, theta_plus_eps)
        theta_minus_eps = q + np.array([0., 0., -self.theta_eps, .0])
        palm_patch_theta_minus, f_1_patch_theta_minus, f_2_patch_theta_minus = \
            gp.extract_rgbd_patches_cfg(rgbd, theta_minus_eps)
        d_patch_palm_d_theta = (np.array(palm_patch_theta_plus) -
                                np.array(palm_patch_theta_minus)) / (2 * self.theta_eps)
        d_patch_f_1_d_theta = (np.array(f_1_patch_theta_plus) -
                               np.array(f_1_patch_theta_minus)) / (2 * self.theta_eps)
        d_patch_f_2_d_theta = (np.array(f_2_patch_theta_plus) -
                               np.array(f_2_patch_theta_minus)) / (2 * self.theta_eps)

        # Gradients of patches with respect to the gripper width
        w_plus_eps = q + np.array([0., 0., 0., self.w_eps])
        palm_patch_w_plus, f_1_patch_w_plus, f_2_patch_w_plus = \
            gp.extract_rgbd_patches_cfg(rgbd, w_plus_eps)
        w_minus_eps = q + np.array([0., 0., 0., -self.w_eps])
        palm_patch_w_minus, f_1_patch_w_minus, f_2_patch_w_minus = \
            gp.extract_rgbd_patches_cfg(rgbd, w_minus_eps)
        d_patch_palm_d_w = (np.array(palm_patch_w_plus) -
                            np.array(palm_patch_w_minus)) / (2 * self.w_eps)
        d_patch_f_1_d_w = (np.array(f_1_patch_w_plus) -
                           np.array(f_1_patch_w_minus)) / (2 * self.w_eps)
        d_patch_f_2_d_w = (np.array(f_2_patch_w_plus) -
                           np.array(f_2_patch_w_minus)) / (2 * self.w_eps)

        # d_prob_d_x = np.sum(np.multiply(d_prob_d_palm, d_patch_palm_d_x)) + \
        #        np.sum(np.multiply(d_prob_d_f_1, d_patch_f_1_d_x)) + \
        #        np.sum(np.multiply(d_prob_d_f_2, d_patch_f_2_d_x))

        # d_prob_d_y = np.sum(np.multiply(d_prob_d_palm, d_patch_palm_d_y)) + \
        #        np.sum(np.multiply(d_prob_d_f_1, d_patch_f_1_d_y)) + \
        #        np.sum(np.multiply(d_prob_d_f_2, d_patch_f_2_d_y))

        # d_prob_d_theta = np.sum(np.multiply(d_prob_d_palm, d_patch_palm_d_theta)) + \
        #        np.sum(np.multiply(d_prob_d_f_1, d_patch_f_1_d_theta)) + \
        #        np.sum(np.multiply(d_prob_d_f_2, d_patch_f_2_d_theta))

        # d_prob_d_w = np.sum(np.multiply(d_prob_d_palm, d_patch_palm_d_w)) + \
        #        np.sum(np.multiply(d_prob_d_f_1, d_patch_f_1_d_w)) + \
        #        np.sum(np.multiply(d_prob_d_f_2, d_patch_f_2_d_w))

        d_prob_d_x_palm = np.multiply(d_prob_d_palm, d_patch_palm_d_x)
        d_prob_d_x_f_1 = np.multiply(d_prob_d_f_1, d_patch_f_1_d_x)
        d_prob_d_x_f_2 = np.multiply(d_prob_d_f_2, d_patch_f_2_d_x)

        d_prob_d_y_palm = np.multiply(d_prob_d_palm, d_patch_palm_d_y)
        d_prob_d_y_f_1 = np.multiply(d_prob_d_f_1, d_patch_f_1_d_y)
        d_prob_d_y_f_2 = np.multiply(d_prob_d_f_2, d_patch_f_2_d_y)

        d_prob_d_theta_palm = np.multiply(d_prob_d_palm, d_patch_palm_d_theta)
        d_prob_d_theta_f_1 = np.multiply(d_prob_d_f_1, d_patch_f_1_d_theta)
        d_prob_d_theta_f_2 = np.multiply(d_prob_d_f_2, d_patch_f_2_d_theta)

        d_prob_d_w_palm = np.multiply(d_prob_d_palm, d_patch_palm_d_w)
        d_prob_d_w_f_1 = np.multiply(d_prob_d_f_1, d_patch_f_1_d_w)
        d_prob_d_w_f_2 = np.multiply(d_prob_d_f_2, d_patch_f_2_d_w)

        d_prob_d_x = np.sum(d_prob_d_x_palm) + np.sum(d_prob_d_x_f_1) + \
            np.sum(d_prob_d_x_f_2)
        d_prob_d_y = np.sum(d_prob_d_y_palm) + np.sum(d_prob_d_y_f_1) + \
            np.sum(d_prob_d_y_f_2)
        d_prob_d_theta = np.sum(d_prob_d_theta_palm) + np.sum(d_prob_d_theta_f_1) + \
            np.sum(d_prob_d_theta_f_2)
        d_prob_d_w = np.sum(d_prob_d_w_palm) + np.sum(d_prob_d_w_f_1) + \
            np.sum(d_prob_d_w_f_2)

        d_prob_d_q = np.array(
            [d_prob_d_x, d_prob_d_y, d_prob_d_theta, d_prob_d_w])
        # print d_prob_d_q

        # if save_grad:
        #     self.plot_rgbd_grad_both(d_prob_d_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_palm')
        #     self.plot_rgbd_grad_both(d_prob_d_f_1, rgbd_id,
        #                              iter_num, 'd_prob_d_f_1')
        #     self.plot_rgbd_grad_both(d_prob_d_f_2, rgbd_id,
        #                              iter_num, 'd_prob_d_f_2')

        #     self.plot_rgbd_grad_both(d_patch_palm_d_x, rgbd_id,
        #                              iter_num, 'd_patch_palm_d_x')
        #     self.plot_rgbd_grad_both(d_patch_f_1_d_x, rgbd_id,
        #                              iter_num, 'd_patch_f_1_d_x')
        #     self.plot_rgbd_grad_both(d_patch_f_2_d_x, rgbd_id,
        #                              iter_num, 'd_patch_f_2_d_x')

        #     self.plot_rgbd_grad_both(d_patch_palm_d_y, rgbd_id,
        #                              iter_num, 'd_patch_palm_d_y')
        #     self.plot_rgbd_grad_both(d_patch_f_1_d_y, rgbd_id,
        #                              iter_num, 'd_patch_f_1_d_y')
        #     self.plot_rgbd_grad_both(d_patch_f_2_d_y, rgbd_id,
        #                              iter_num, 'd_patch_f_2_d_y')

        #     self.plot_rgbd_grad_both(d_patch_palm_d_theta, rgbd_id,
        #                              iter_num, 'd_patch_palm_d_theta')
        #     self.plot_rgbd_grad_both(d_patch_f_1_d_theta, rgbd_id,
        #                              iter_num, 'd_patch_f_1_d_theta')
        #     self.plot_rgbd_grad_both(d_patch_f_2_d_theta, rgbd_id,
        #                              iter_num, 'd_patch_f_2_d_theta')

        #     self.plot_rgbd_grad_both(d_patch_palm_d_w, rgbd_id,
        #                              iter_num, 'd_patch_palm_d_w')
        #     self.plot_rgbd_grad_both(d_patch_f_1_d_w, rgbd_id,
        #                              iter_num, 'd_patch_f_1_d_w')
        #     self.plot_rgbd_grad_both(d_patch_f_2_d_w, rgbd_id,
        #                              iter_num, 'd_patch_f_2_d_w')

        #     self.plot_rgbd_grad_both(d_prob_d_x_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_x_palm')
        #     self.plot_rgbd_grad_both(d_prob_d_x_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_x_f_1')
        #     self.plot_rgbd_grad_both(d_prob_d_x_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_x_f_2')

        #     self.plot_rgbd_grad_both(d_prob_d_y_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_y_palm')
        #     self.plot_rgbd_grad_both(d_prob_d_y_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_y_f_1')
        #     self.plot_rgbd_grad_both(d_prob_d_y_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_y_f_2')

        #     self.plot_rgbd_grad_both(d_prob_d_theta_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_theta_palm')
        #     self.plot_rgbd_grad_both(d_prob_d_theta_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_theta_f_1')
        #     self.plot_rgbd_grad_both(d_prob_d_theta_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_theta_f_2')

        #     self.plot_rgbd_grad_both(d_prob_d_w_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_w_palm')
        #     self.plot_rgbd_grad_both(d_prob_d_w_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_w_f_1')
        #     self.plot_rgbd_grad_both(d_prob_d_w_palm, rgbd_id,
        #                              iter_num, 'd_prob_d_w_f_2')

        return d_prob_d_q, suc_prob

    def projection_config(self, q):
        if q[0] < .0:
            q[0] = .0
        if q[0] >= self.rgbd_cols:
            q[0] = self.rgbd_cols - 1
        if q[1] < .0:
            q[1] = .0
        if q[1] >= self.rgbd_rows:
            q[1] = self.rgbd_rows - 1
        q[2] %= 2 * np.pi
        if q[3] < self.min_grasp_width:
            q[3] = self.min_grasp_width
        if q[3] > self.max_grasp_width:
            q[3] = self.max_grasp_width
        return q

    def find_learning_rate_bt(self, rgbd, q, suc_prob, grad_q, line_search_log=None, use_talor=None):
        t = time.time()
        iter_num = -1

        # Different alpha for each link angle?
        alpha = 0.05
        #alpha = 0.01
        #alpha = np.array([1., 1., 0.5, 1.])
        #alpha = np.array([5., 5., 0.05, 2.])
        #alpha = 10*np.array([2., 2., 0.005, 2.])
        #alpha = np.array([0.01, 0.01, 0.0005, 0.01])
        tao = 0.5
        beta = 0.1
        l = 0
        #iter_limit = 100
        iter_limit = 2
        q_new = q + alpha * grad_q
        # line_search_log.writelines('q_new: ' + str(q_new))
        # line_search_log.writelines('\n')
        q_new = self.projection_config(q_new)
        # line_search_log.writelines('q_new after projection: ' + str(q_new))
        # line_search_log.writelines('\n')
        suc_prob_new = self.get_pred(rgbd, q_new)
        talor_1st_order = beta * alpha * np.inner(grad_q, grad_q)
        # Double check the mean is the right thing to do or not?
        talor_1st_order = np.mean(talor_1st_order)
        # line_search_log.writelines('use_talor: ' + str(use_talor))
        # line_search_log.writelines('\n')
        # print suc_prob_new, suc_prob, talor_1st_order
        # print type(suc_prob_new), type(suc_prob), type(use_talor), type(talor_1st_order)
        # while suc_prob_new <= suc_prob + use_talor * talor_1st_order:
        while suc_prob_new <= suc_prob:
            # line_search_log.writelines('l: ' + str(l))
            # line_search_log.writelines('\n')
            # line_search_log.writelines('suc_prob_new: ' + str(suc_prob_new))
            # line_search_log.writelines('\n')
            # line_search_log.writelines('suc_prob: ' + str(suc_prob))
            # line_search_log.writelines('\n')
            # line_search_log.writelines('talor_1st_order: ' + str(talor_1st_order))
            # line_search_log.writelines('\n')
            # line_search_log.writelines('alpha: ' + str(alpha))
            # line_search_log.writelines('\n')
            alpha *= tao
            q_new = q + alpha * grad_q
            # line_search_log.writelines('q_new: ' + str(q_new))
            # line_search_log.writelines('\n')
            q_new = self.projection_config(q_new)
            # line_search_log.writelines('q_new after projection: ' + str(q_new))
            # line_search_log.writelines('\n')
            suc_prob_new = self.get_pred(rgbd, q_new)
            talor_1st_order = beta * alpha * np.inner(grad_q, grad_q)
            if l > iter_limit:
                # line_search_log.writelines('********* Can not find alpha in ' + str(iter_limit) + ' iters')
                # line_search_log.writelines('\n')
                alpha = 0.
                break
            l += 1
        # line_search_log.writelines('Line search time: ' + str(time.time() - t))
        # line_search_log.writelines('\n')
        return alpha

    def grad_descent_inf(self, rgbd, q, rgbd_id=-1, grasp_id=-1):

        config_grad_path = '/home/hashb/tmp'
        config_grad_path += '/config_line_search_limits/rgbd_' + \
            str(rgbd_id) + '_grasp_' + str(grasp_id) + '/'
        if not os.path.exists(config_grad_path):
            os.makedirs(config_grad_path)
        # log_file_path = config_grad_path + 'log'
        # log_file = open(# log_file_path, 'w')
        # line_search_log_file_path = config_grad_path + 'log_ls'
        # line_search_log = open(line_search_log_file_path, 'w')

        t = time.time()

        suc_probs = []
        iter_total_num = 10
        delta = 10**-6
        use_talor = 0.
        # if grasp_id % 2 == 1:
        #    use_talor = 1.

        save_grad = False
        # if rgbd_id % 10 != 0:
        #    save_grad = False

        for iter_num in xrange(iter_total_num):
            grad_q, suc_prob = self.compute_grad(
                rgbd, q, save_grad, None, iter_num)
            suc_probs.append(suc_prob)
            grad_norm = np.linalg.norm(grad_q)
            # log_file.writelines('iter: ' + str(iter_num))
            # log_file.writelines('\n')
            # log_file.writelines('q: ' + str(q))
            # log_file.writelines('\n')
            # log_file.writelines('grad_q: ' + str(grad_q))
            # log_file.writelines('\n')
            # log_file.writelines('norm(grad_q): ' + str(grad_norm))
            # log_file.writelines('\n')
            # log_file.writelines('suc_prob: ' + str(suc_prob))
            # log_file.writelines('\n')
            # Stop if gradient is too small
            if grad_norm < delta:
                # log_file.writelines('Gradient too small, stop iteration!\n')
                break

            # line_search_log.writelines('iter: ' + str(iter_num))
            # line_search_log.writelines('\n')
            q_learn_rate = self.find_learning_rate_bt(
                rgbd, q, suc_prob, grad_q, line_search_log=None, use_talor=use_talor)
            # line_search_log.writelines('######################################################')
            # line_search_log.writelines('\n')
            # log_file.writelines('q_learn_rate: ' + str(q_learn_rate))
            # log_file.writelines('\n')
            # if q_learn_rate == 0.:
            #     break
            q_update = q_learn_rate * grad_q
            q_update = q + q_update
            # log_file.writelines('q: ' + str(q_update))
            # log_file.writelines('\n')
            q_update = self.projection_config(q_update)
            # log_file.writelines('q after projection: ' + str(q_update))
            # log_file.writelines('\n')
            q_update_proj = q_update - q
            if np.linalg.norm(q_update_proj) < delta:
                # log_file.writelines('q_update_proj too small, stop iteration.')
                # log_file.writelines('\n')
                break
            q = q_update

        suc_probs = np.array(suc_probs)
        # plt.plot(suc_probs)
        # plt.ylabel('Suc Probalities')
        # plt.xlabel('Iteration')
        # plt.savefig(config_grad_path + 'suc_prob.png')
        # plt.cla()
        # plt.clf()
        # plt.close()

        # elapased_time = time.time() - t
        # log_file.writelines(str(elapased_time))
        # log_file.writelines('\n')
        # log_file.close()
        # line_search_log.close()

        return q, suc_probs[-1], suc_probs[0]
