# Code is based on
# http://dsp.stackexchange.com/questions/2287/how-to-fit-an-ellipse-to-2d-data
import numpy

# Generate some points distributed uniformely along an ellipse
x = 2 * (numpy.random.rand(10000, 1) - 0.5)
y = 2 * (numpy.random.rand(10000, 1) - 0.5)
d = (x / 0.5) ** 2 + (y / 0.25) ** 2
inside = numpy.where(d < 1)[0]
x = x[inside]
y = y[inside]
data = numpy.hstack((x, y)).T

# Now rotate by 0.5 rad and translate it to (4, -8)
angle = 0.5
rotation = numpy.array([
    [numpy.cos(0.4), -numpy.sin(0.4)],
    [numpy.sin(0.4), numpy.cos(0.4)]])

data = numpy.dot(rotation, data)
data[0, :] += 4
data[1, :] -= 8

raw_data = data.copy()
# Step 1: center the data to get the translation vector.
print 'Translation', data.mean(axis=1)
data -= numpy.reshape(data.mean(axis=1), (2, 1))

# Step 2: perform PCA to find rotation matrix.
scatter = numpy.dot(data, data.T)
eigenvalues, transform = numpy.linalg.eig(scatter)
print 'eigenvalues:', eigenvalues
print 'Rotation matrix', transform

# Step 3: Rotate back the data and compute radii.
# You can also get the radii from smaller to bigger
# with 2 / numpy.sqrt(eigenvalues)
rotated = numpy.dot(numpy.linalg.inv(transform), data)
radii = 2 * rotated.std(axis=1)
print 'Radii', radii

from matplotlib.patches import Ellipse
import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np

#mean = [ 19.92977907 ,  5.07380955]
#width = 30
#height = 1.01828848
#angle = 120
mean = raw_data.mean(axis=1)
width = 2 * radii[0]
height = 2 * radii[1]
angle = np.degrees(np.arctan2(transform[1, 0], transform[0, 0]))
print angle
ell = mpl.patches.Ellipse(xy=mean, width=width, height=height, angle=angle)
ell.set_alpha(0.1)
fig, ax = plt.subplots()
print raw_data
ax.scatter(raw_data[0], raw_data[1])
ax.add_patch(ell)
ax.set_aspect('equal')
ax.autoscale()
plt.show()
