import cv2
import numpy as np

#bg_img_path = '/media/kai/cornell_grasp_data/backgrounds/pcdb0005r.png'
bg_img_path = '/media/kai/cornell_grasp_data/backgrounds/pcdb0002r.png'
#bg_img = cv2.imread(bg_img_path, cv2.IMREAD_COLOR)
bg_img = cv2.imread(bg_img_path, cv2.IMREAD_GRAYSCALE)

#img_path = '/media/kai/cornell_grasp_data/04/pcd0480r.png'
img_path = '/media/kai/cornell_grasp_data/01/pcd0100r.png'
#img = cv2.imread(img_path, cv2.IMREAD_COLOR)
img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

fg_img = img - bg_img
print np.median(fg_img)
fg_img[(fg_img < 20)] = 0
fg_img[(fg_img > 220)] = 0
#fg_img[:480/5, :] = 0
#fg_img[480*4/5:, :] = 0
#fg_img[:, :640/5] = 0
#fg_img[:, 640*4/5:] = 0
fg_img[:480 / 4, :] = 0
fg_img[480 * 3 / 4:, :] = 0
fg_img[:, :640 / 4] = 0
fg_img[:, 640 * 3 / 4:] = 0
cv2.imshow('fg_img', fg_img)
cv2.waitKey(0)
print fg_img

img = cv2.imread(img_path, cv2.IMREAD_COLOR)
ret, thresh = cv2.threshold(fg_img, 20, 255, 0)
contours, hierarchy = cv2.findContours(thresh, 1, 2)
max_cnt_id = np.argmax([len(x) for x in contours])
ellipse = cv2.fitEllipse(contours[max_cnt_id])
print ellipse
cv2.ellipse(img, ellipse, (0, 255, 0), 2)
(x, y), (MA, ma), angle = ellipse
print x, y, MA, ma, angle

# print len(contours)
# for cnt in contours:
#    print len(cnt)
#    if len(cnt) < 20:
#        continue
#    ellipse = cv2.fitEllipse(cnt)
#    cv2.ellipse(img,ellipse,(0,255,0),2)
#
#fg_points = np.argwhere(fg_img > 20)
# print fg_points
#fg_points = fg_points[:, ::-1]
# print fg_points
#ellipse = cv2.fitEllipse(fg_points)
# cv2.ellipse(img,ellipse,(0,0,255),2)

cv2.imshow('img', img)
cv2.waitKey(0)


#bg_img_path = '/media/kai/cornell_grasp_data/backgrounds/pcdb0005r.png'
##bg_img_path = '/media/kai/cornell_grasp_data/backgrounds/pcdb0002r.png'
#bg_img = cv2.imread(bg_img_path, cv2.IMREAD_COLOR)
##bg_img = cv2.imread(bg_img_path, cv2.IMREAD_GRAYSCALE)
#
#img_path = '/media/kai/cornell_grasp_data/04/pcd0480r.png'
##img_path = '/media/kai/cornell_grasp_data/01/pcd0145r.png'
#img = cv2.imread(img_path, cv2.IMREAD_COLOR)
##img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
#
#hsv_bg = cv2.cvtColor(bg_img, cv2.COLOR_BGR2HSV)
#hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
#hsv_fg = hsv_img - hsv_bg
#lower_blue = np.array([30,30,0])
#upper_blue = np.array([220,220,255])
#mask = cv2.inRange(hsv_fg, lower_blue, upper_blue)
#img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#fg_img = cv2.bitwise_and(img_gray,img_gray, mask= mask)
#
#fg_img[:480/4, :] = 0
#fg_img[480*3/4:, :] = 0
#fg_img[:, :640/4] = 0
#fg_img[:, 640*3/4:] = 0
#
#ret,thresh = cv2.threshold(fg_img,1,255,0)
#contours, hierarchy = cv2.findContours(thresh, 1, 2)
##coutour_sizes = [len(x) for x in coutours]
# print len(contours)
# for cnt in contours:
#    print len(cnt)
#    if len(cnt) < 20:
#        continue
#    ellipse = cv2.fitEllipse(cnt)
#    cv2.ellipse(img,ellipse,(0,255,0),2)
#
#
#cv2.imshow('mask', mask)
##cv2.imshow('img', img)
# cv2.waitKey(0)
