import h5py
import cv2
import numpy as np

rgbd_file = h5py.File(
    '/media/kai/tars_HDD/cornell_grasp_data/h5_data/rgbd.h5', 'r')

rgbd = rgbd_file['RGBD'][45]
print rgbd.shape
print rgbd[:, :, :3].shape
print rgbd[:, :, :3].dtype
#rgbd_file = '/media/kai/tars_HDD/cornell_grasp_data/01/pcd0100r.png'
#rgbd = cv2.imread(rgbd_file, cv2.IMREAD_COLOR)
# print rgbd.dtype

rows, cols = rgbd.shape[0:2]
M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 90, 1)

# RGB
rgb = rgbd[:, :, :3].astype('uint8')
#cv2.imshow('rgbd', rgbd[:, :, :3].astype('uint8'))
cv2.imwrite('../images/rgb.png', rgb)
# cv2.waitKey(0)

dst = cv2.warpAffine(rgb, M, (cols, rows))

#cv2.imshow('rgbd2', dst[:, :, :3].astype('uint8'))
cv2.imwrite('../images/rgb_rot.png', dst)
# cv2.waitKey(0)

# normalize
#depth_info = rgbd[:, :, 4:]
# depth_info = np.array(map(lambda x: , depth_info))

# Depth
depth = rgbd[:, :, 3]
print 'max, min depth:', np.max(depth), np.min(depth)
depth = 255. * (depth - np.min(depth)) / (np.max(depth) - np.min(depth))
# print depth
cv2.imwrite('../images/depth.png', depth)

depth_dst = cv2.warpAffine(depth, M, (cols, rows))

cv2.imwrite('../images/depth_rot.png', depth_dst)

# Normal
normal = rgbd[:, :, 4:7]
for i in xrange(3):
    normal[:, :, i] = 255. * (normal[:, :, i] - np.min(normal[:, :, i])) / \
        (np.max(normal[:, :, i]) - np.min(normal[:, :, i]))
# print normal
normal = normal.astype('uint8')
cv2.imwrite('../images/normal.png', normal)

normal_dst = cv2.warpAffine(normal, M, (cols, rows))

cv2.imwrite('../images/normal_rot.png', normal_dst)

# Curvature
curv = rgbd[:, :, 7]
curv = 255. * (curv - np.min(curv)) / (np.max(curv) - np.min(curv))
# print curv
cv2.imwrite('../images/curv.png', curv)

curv_dst = cv2.warpAffine(curv, M, (cols, rows))

cv2.imwrite('../images/curv_rot.png', curv_dst)


rgbd_file.close()
