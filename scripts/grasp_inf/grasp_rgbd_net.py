import tensorflow as tf
import random
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import tsne_vis
import h5py
import numpy as np
import time
#import gen_rgbd_patches as gp
import cv2


class GraspRgbdNet:

    def __init__(self):
        # Parameters
        self.model_path = "/home/hashb/baxter_ws/src/baxter_pipeline/scripts"

        self.palm_patch_rows = 200
        self.palm_patch_cols = 200
        self.finger_patch_rows = 100
        self.finger_patch_cols = 100
        self.rgbd_channels = 8
        #self.alpha = 0.5
        #self.rate_dec_epochs = 40000
        #self.training_epochs = 120000
        self.rate_dec_epochs = 10000
        self.training_epochs = 20000
        self.batch_size = 16
        self.display_step = 1  # 1000
        #self.dropout_prob = 0.7
        #self.dropout_prob = 0.5
        self.dropout_prob = 1.0
        #self.classes_num = 2
        self.classes_num = 1
        #self.read_data_ratio = 0.25
        #self.read_batch_epochs = 5000
        self.read_data_ratio = 0.5
        #self.read_batch_epochs = 10000
        self.read_batch_epochs = 5000

        # tf Graph input
        self.holder_palm = tf.placeholder(tf.float32,
                                          [None, self.palm_patch_rows, self.palm_patch_cols, self.rgbd_channels], name='holder_palm')
        self.holder_f_1 = tf.placeholder(tf.float32,
                                         [None, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels], name='holder_f_1')
        self.holder_f_2 = tf.placeholder(tf.float32,
                                         [None, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels], name='holder_f_2')
        self.holder_labels = tf.placeholder(
            tf.float32, [None, self.classes_num], name='holder_labels')
        #self.holder_labels = tf.placeholder(tf.int64, [None], name = 'holder_labels')
        self.keep_prob = tf.placeholder(tf.float32, name='holder_keep_prob')
        self.learning_rate = tf.placeholder(
            tf.float32, name='holder_learn_rate')

        # Create some wrappers for simplicity
    def conv2d(self, x, W, b, stride=1):
        # Conv2D wrapper, with relu activation
        x = tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='SAME')
        x = tf.nn.bias_add(x, b)
        return tf.nn.relu(x)

    def max_pool2d(self, x, k, stride=1):
        # MaxPool2D wrapper
        return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, stride, stride, 1],
                              padding='SAME')

    # CNN model for each finger/palm voxel
    def conv_net_finger(self, x, weights, biases):
        # Convolution 1
        conv1 = self.conv2d(x, weights['conv1'], biases['conv1'], 2)
        # Convolution 2
        conv2 = self.conv2d(conv1, weights['conv2'], biases['conv2'], 2)
        # Max pool layer
        max_pool = self.max_pool2d(conv2, 2, 2)
        # FC layer 1
        fc1 = tf.add(tf.matmul(tf.reshape(max_pool, [-1, int(np.prod(max_pool.get_shape()[1:]))]),
                               weights['fc1']), biases['fc1'])
        fc1 = tf.nn.relu(fc1)
        fc1 = tf.nn.dropout(fc1, self.keep_prob)
        return fc1
       # #FC layer 2
       # fc2 = tf.add(tf.matmul(fc1, weights['fc2']), biases['fc2'])
       # fc2 = tf.nn.relu(fc2)
       # fc2 = tf.nn.dropout(fc2, keep_prob)

       # return fc2

    def conv_net_hand(self, x_palm, x_f_1, x_f_2, w_palm, b_palm,
                      w_f_1, b_f_1, w_f_2, b_f_2, w_hand, b_hand):
        with tf.name_scope('Palm'):
            fc_palm = self.conv_net_finger(x_palm, w_palm, b_palm)
        with tf.name_scope('F_1'):
            fc_f_1 = self.conv_net_finger(x_f_1, w_f_1, b_f_1)
        with tf.name_scope('F_2'):
            fc_f_2 = self.conv_net_finger(x_f_2, w_f_2, b_f_2)
        with tf.name_scope('Concat'):
            fc_concat = tf.concat([fc_palm, fc_f_1, fc_f_2], 1)
        with tf.name_scope('Hand'):
            # fc1 layer for concatenated fingers and palm features
            fc1 = tf.add(tf.matmul(fc_concat, w_hand['fc1']), b_hand['fc1'])
            #fc1 = tf.matmul(fc_concat, w_hand['fc1'])
            fc1 = tf.nn.relu(fc1)
            fc1 = tf.nn.dropout(fc1, self.keep_prob)
            # Output layer with linear activation
            #out_layer = tf.nn.softmax(tf.matmul(fc1, w_hand['out']) + b_hand['out'])
            #out_layer = tf.nn.relu(tf.matmul(fc1, w_hand['out']) + b_hand['out'])
            out_layer = tf.matmul(fc1, w_hand['out']) + b_hand['out']
            #out_layer = tf.matmul(fc1, w_hand['out'])

        return (fc_concat, fc1, out_layer)

    def create_net_var(self):
        # Store layers weight & bias
        # conv shapes for the palm.
        # 6x6x6 conv, 8 input channels, 32 outputs
        conv1_w_par_palm = [12, 12, self.rgbd_channels, 32]
        conv1_b_par_palm = [32]
        # 3x3x3 conv, 32 input channels, 8 outputs
        conv2_w_par_palm = [6, 6, 32, 8]
        conv2_b_par_palm = [8]
        # fc1, 2(conv1)*2(conv2)*2(max pool) = 8
        # 200/8 * 200/8 * 8 inputs, 32 outputs
        fc1_w_par_palm = [self.palm_patch_rows * self.palm_patch_cols / 8, 32]

        # conv filter shapes for fingers.
        # 6x6x6 conv, 1 input channels, 32 outputs
        conv1_w_par = [6, 6, self.rgbd_channels, 32]
        conv1_b_par = [32]
        # 3x3x3 conv, 32 input channels, 8 outputs
        conv2_w_par = [3, 3, 32, 8]
        conv2_b_par = [8]
        # fc1, 2(conv1)*2(conv2)*2(max pool) = 8
        # ceil(100/8) * ceil(100/8) * 8 inputs, 32 outputs
        fc1_w_par = [int(np.ceil(self.finger_patch_rows / 8.)
                         * np.ceil(self.finger_patch_cols / 8.) * 8), 32]
        # fc2, 32 inputs, 16 outputs
        #fc2_w_par = [32, 16]

        fc1_b_par = [32]
        #fc2_b_par = [16]

        self.weights_palm = {
            'conv1': tf.get_variable(name='w_conv1_palm', shape=conv1_w_par_palm,
                                     initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            'conv2': tf.get_variable(name='w_conv2_palm', shape=conv2_w_par_palm,
                                     initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            'fc1': tf.get_variable(name='w_fc1_palm', shape=fc1_w_par_palm,
                                   initializer=tf.contrib.layers.xavier_initializer()),
        }

        self.biases_palm = {
            'conv1': tf.get_variable(name='b_conv1_palm', shape=conv1_b_par_palm,
                                     initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
            'conv2': tf.get_variable(name='b_conv2_palm', shape=conv2_b_par_palm,
                                     initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
            'fc1': tf.get_variable(name='b_fc1_palm', shape=fc1_b_par,
                                   initializer=tf.random_normal_initializer()),
        }

        self.weights_f_1 = {
            'conv1': tf.get_variable(name='w_conv1_f_1', shape=conv1_w_par,
                                     initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            'conv2': tf.get_variable(name='w_conv2_f_1', shape=conv2_w_par,
                                     initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            'fc1': tf.get_variable(name='w_fc1_f_1', shape=fc1_w_par,
                                   initializer=tf.contrib.layers.xavier_initializer()),
        }

        self.biases_f_1 = {
            'conv1': tf.get_variable(name='b_conv1_f_1', shape=conv1_b_par,
                                     initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
            'conv2': tf.get_variable(name='b_conv2_f_1', shape=conv2_b_par,
                                     initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
            'fc1': tf.get_variable(name='b_fc1_f_1', shape=fc1_b_par,
                                   initializer=tf.random_normal_initializer()),
        }

        self.weights_f_2 = {
            'conv1': tf.get_variable(name='w_conv1_f_2', shape=conv1_w_par,
                                     initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            'conv2': tf.get_variable(name='w_conv2_f_2', shape=conv2_w_par,
                                     initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            'fc1': tf.get_variable(name='w_fc1_f_2', shape=fc1_w_par,
                                   initializer=tf.contrib.layers.xavier_initializer()),
        }

        self.biases_f_2 = {
            'conv1': tf.get_variable(name='b_conv1_f_2', shape=conv1_b_par,
                                     initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
            'conv2': tf.get_variable(name='b_conv2_f_2', shape=conv2_b_par,
                                     initializer=tf.constant_initializer(0.0)),
            'fc1': tf.get_variable(name='b_fc1_f_2', shape=fc1_b_par,
                                   initializer=tf.random_normal_initializer()),
        }

        self.weights_hand = {
            # fc1 32*3=96, 96 inputs, 32 outputs
            'fc1': tf.get_variable(name='w_fc1_hand', shape=[96, 32],
                                   initializer=tf.contrib.layers.xavier_initializer_conv2d()),
            # output layer, 32 inputs, 2 output
            'out': tf.get_variable(name='w_out_hand', shape=[32, self.classes_num],
                                   initializer=tf.contrib.layers.xavier_initializer()),
        }

        self.biases_hand = {
            'fc1': tf.get_variable(name='b_fc1_hand', shape=[32],
                                   initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
            'out': tf.get_variable(name='b_out_hand', shape=[self.classes_num],
                                   initializer=tf.constant_initializer(0.0)),
            # initializer=tf.random_normal_initializer()),
        }

    def cost_function(self):
        with tf.name_scope('Pred'):
            (fc_concat_hand, fc1_hand, logits) = self.conv_net_hand(self.holder_palm, self.holder_f_1, self.holder_f_2,
                                                                    self.weights_palm, self.biases_palm, self.weights_f_1, self.biases_f_1,
                                                                    self.weights_f_2, self.biases_f_2, self.weights_hand, self.biases_hand)
            self.pred = tf.nn.sigmoid(logits)
            #self.pred = logits

        with tf.name_scope('F2Vis'):
            self.feature_to_vis = tf.concat([fc_concat_hand, fc1_hand], 1)

        with tf.name_scope('Pred_error'):
            self.pred_error = tf.reduce_mean(
                tf.abs(self.holder_labels - self.pred))

        with tf.name_scope('Pred_error_test'):
            self.pred_error_test = tf.reduce_mean(
                tf.abs(self.holder_labels - self.pred))

        with tf.name_scope('Cost'):
            # logistic regression (or cross entropy) loss
            # http://scikit-learn.org/stable/modules/linear_model.html#logistic-regression
            # http://stackoverflow.com/questions/12578336/why-is-the-bias-term-not-regularized-in-ridge-regression
            # http://www.ritchieng.com/machine-learning/deep-learning/tensorflow/regularization/
            #self.cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits, self.holder_labels))
            #self.cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits, self.holder_labels))
            # https://www.tensorflow.org/api_docs/python/nn/classification#sigmoid_cross_entropy_with_logits
            self.cost = tf.reduce_mean(
                tf.nn.sigmoid_cross_entropy_with_logits(logits, self.holder_labels))

        with tf.name_scope('SGD'):
            self.optimizer = tf.train.AdamOptimizer(
                learning_rate=self.learning_rate).minimize(self.cost)

        # Create a summary to monitor cost
        loss_sum = tf.scalar_summary('loss', self.cost)
        # Create a summary to monitor training prediction errors
        pred_err_sum = tf.scalar_summary('pred_err_train', self.pred_error)
        # Learnining rate
        learn_rate_sum = tf.scalar_summary('learning_rate', self.learning_rate)
        self.train_summary = tf.merge_summary(
            [loss_sum, pred_err_sum, learn_rate_sum])

        # Testing prediction errors
        self.pred_err_test_sum = tf.scalar_summary(
            'pred_err_test', self.pred_error_test)

    def read_rgbd_data(self):
        data_path = '/home/hashb/tmp/cornell_grasp_data/'
        patch_file = h5py.File(data_path + 'h5_data/patch.h5', 'r')

        #grasp_labels = np.copy(patch_file['labels']).astype(int)
        grasps_num = patch_file['labels'].shape[0]
        #grasps_num /= 2

        grasp_labels = np.zeros((grasps_num, self.classes_num))
        failure_grasp_idx = (np.array(patch_file['labels']) == -1.)
        #failure_grasp_idx = (np.array(patch_file['labels'])[grasps_num:] == -1.)
        grasp_labels[-failure_grasp_idx, 0] = 1.
        print grasp_labels
        print 'grasp_labels.shape: ', grasp_labels.shape
        print 'positive #:', np.sum(grasp_labels[:, 0] == 1)
        print 'negative #:', np.sum(grasp_labels[:, 0] == 0)

        palm_rgbd = np.zeros(
            (grasps_num, self.palm_patch_rows, self.palm_patch_cols, self.rgbd_channels))
        f_1_rgbd = np.zeros((grasps_num, self.finger_patch_rows,
                             self.finger_patch_cols, self.rgbd_channels))
        f_2_rgbd = np.zeros((grasps_num, self.finger_patch_rows,
                             self.finger_patch_cols, self.rgbd_channels))

        #grasps_num = 200
        for i in xrange(grasps_num):
            # print 'reading ', i
            palm_rgbd[i] = patch_file['palm_' + str(i)]
            f_1_rgbd[i] = patch_file['f_1_' + str(i)]
            f_2_rgbd[i] = patch_file['f_2_' + str(i)]
            #palm_rgbd[i] = patch_file['palm_' + str(i + grasps_num)]
            #f_1_rgbd[i] = patch_file['f_1_' + str(i + grasps_num)]
            #f_2_rgbd[i] = patch_file['f_2_' + str(i + grasps_num)]

        print 'reading is done.'
        patch_file.close()

        self.train_samples_num = int(grasps_num * 0.9)
        self.testing_samples_num = grasps_num - self.train_samples_num

        self.training_labels = grasp_labels[0:self.train_samples_num]
        self.training_samples_palm = palm_rgbd[0:self.train_samples_num, :]
        self.training_samples_f_1 = f_1_rgbd[0:self.train_samples_num, :]
        self.training_samples_f_2 = f_2_rgbd[0:self.train_samples_num, :]

        self.testing_labels = grasp_labels[self.train_samples_num:grasps_num]
        self.testing_samples_palm = palm_rgbd[
            self.train_samples_num:grasps_num, :]
        self.testing_samples_f_1 = f_1_rgbd[
            self.train_samples_num:grasps_num, :]
        self.testing_samples_f_2 = f_2_rgbd[
            self.train_samples_num:grasps_num, :]

        print self.training_samples_palm.shape
        print self.training_samples_f_1.shape
        print self.training_samples_f_2.shape
        print self.training_labels.shape
        print 'training positive #:', np.sum(self.training_labels[:, 0] == 1)

    def read_rgbd_data_batch(self, batch_ratio=1.):
        data_path = '/home/hashb/tmp/cornell_grasp_data/'
        patch_file = h5py.File(data_path + 'h5_data/patch.h5', 'r')

        print patch_file['labels']
        print patch_file['labels'].shape
        grasps_num = patch_file['labels'].shape[0]
        grasps_num = 1000

        train_pct = 0.9
        batch_grasps_num = int(grasps_num * batch_ratio)
        self.train_samples_num = int(batch_grasps_num * train_pct)
        self.testing_samples_num = batch_grasps_num - self.train_samples_num

        self.training_labels = np.zeros(
            (self.train_samples_num, self.classes_num))
        self.training_samples_palm = np.zeros(
            (self.train_samples_num, self.palm_patch_rows, self.palm_patch_cols, self.rgbd_channels))
        self.training_samples_f_1 = np.zeros(
            (self.train_samples_num, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels))
        self.training_samples_f_2 = np.zeros(
            (self.train_samples_num, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels))

        self.testing_labels = np.zeros(
            (self.testing_samples_num, self.classes_num))
        self.testing_samples_palm = np.zeros(
            (self.testing_samples_num, self.palm_patch_rows, self.palm_patch_cols, self.rgbd_channels))
        self.testing_samples_f_1 = np.zeros(
            (self.testing_samples_num, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels))
        self.testing_samples_f_2 = np.zeros(
            (self.testing_samples_num, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels))

        train_samples_total_num = int(grasps_num * train_pct)
        train_batch_indices = random.sample(
            range(0, train_samples_total_num), self.train_samples_num)
        test_batch_indices = random.sample(
            range(train_samples_total_num, grasps_num), self.testing_samples_num)
        print 'train_batch_indices: ', train_batch_indices
        print np.min(train_batch_indices), np.max(train_batch_indices)
        print 'test_batch_indices: ', test_batch_indices
        print np.min(test_batch_indices), np.max(test_batch_indices)

        for i, grasp_idx in enumerate(train_batch_indices):
            # print 'read train sample: ', i, grasp_idx
            self.training_samples_palm[i] = patch_file[
                'palm_' + str(grasp_idx)]
            self.training_samples_f_1[i] = patch_file['f_1_' + str(grasp_idx)]
            self.training_samples_f_2[i] = patch_file['f_2_' + str(grasp_idx)]
            self.training_labels[i, 0] = patch_file['labels'][grasp_idx]

        for i, grasp_idx in enumerate(test_batch_indices):
            # print 'read test sample: ', i, grasp_idx
            self.testing_samples_palm[i] = patch_file['palm_' + str(grasp_idx)]
            self.testing_samples_f_1[i] = patch_file['f_1_' + str(grasp_idx)]
            self.testing_samples_f_2[i] = patch_file['f_2_' + str(grasp_idx)]
            self.testing_labels[i, 0] = patch_file['labels'][grasp_idx]

        # for i, grasp_idx in enumerate(train_batch_indices):
        #    print 'read train sample: ', i, grasp_idx
        #    if i % 2 == 0:
        #        self.training_samples_palm[i] = patch_file['palm_' + str(grasp_idx)]
        #        self.training_samples_f_1[i] = patch_file['f_1_' + str(grasp_idx)]
        #        self.training_samples_f_2[i] = patch_file['f_2_' + str(grasp_idx)]
        #        self.training_labels[i, 0] = patch_file['labels'][grasp_idx]
        #    else:
        #        self.training_samples_palm[i] = patch_file['palm_' + str(grasp_idx)]
        #        self.training_samples_f_1[i] = patch_file['f_2_' + str(grasp_idx)]
        #        self.training_samples_f_2[i] = patch_file['f_1_' + str(grasp_idx)]
        #        self.training_labels[i, 0] = patch_file['labels'][grasp_idx]

        # for i, grasp_idx in enumerate(test_batch_indices):
        #    print 'read test sample: ', i, grasp_idx
        #    if i % 2 == 0:
        #        self.testing_samples_palm[i] = patch_file['palm_' + str(grasp_idx)]
        #        self.testing_samples_f_1[i] = patch_file['f_1_' + str(grasp_idx)]
        #        self.testing_samples_f_2[i] = patch_file['f_2_' + str(grasp_idx)]
        #        self.testing_labels[i, 0] = patch_file['labels'][grasp_idx]
        #    else:
        #        self.testing_samples_palm[i] = patch_file['palm_' + str(grasp_idx)]
        #        self.testing_samples_f_1[i] = patch_file['f_2_' + str(grasp_idx)]
        #        self.testing_samples_f_2[i] = patch_file['f_1_' + str(grasp_idx)]
        #        self.testing_labels[i, 0] = patch_file['labels'][grasp_idx]

        print 'reading is done.'
        patch_file.close()

        self.training_labels[self.training_labels == -1] = 0
        self.testing_labels[self.testing_labels == -1] = 0

        print self.training_samples_palm.shape
        print self.training_samples_f_1.shape
        print self.training_samples_f_2.shape
        print self.training_labels.shape
        print 'training positive #:', np.sum(self.training_labels[:, 0] == 1)

        print self.testing_samples_palm.shape
        print self.testing_samples_f_1.shape
        print self.testing_samples_f_2.shape
        print self.testing_labels.shape
        print 'testing positive #:', np.sum(self.testing_labels[:, 0] == 1)

    def feed_dict_func(self, train, l_rate=.0, batch_idx=None):
        if train:
            rand_indices = random.sample(
                range(0, self.train_samples_num), self.batch_size)
            ys = self.training_labels[rand_indices]
            xs_palm = self.training_samples_palm[rand_indices]
            xs_f_1 = self.training_samples_f_1[rand_indices]
            xs_f_2 = self.training_samples_f_2[rand_indices]

            for i in xrange(self.batch_size):
                x_palm = xs_palm[i, :, :, :3]
                x_f_1 = xs_f_1[i, :, :, :3]
                x_f_2 = xs_f_2[i, :, :, :3]
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_palm_rgb.png', x_palm.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_1_rgb.png', x_f_1.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_2_rgb.png', x_f_2.astype(int))
                x_palm = xs_palm[i, :, :, 3]
                x_palm = 255. * (x_palm - np.min(x_palm)) / \
                    (np.max(x_palm) - np.min(x_palm))
                x_f_1 = xs_f_1[i, :, :, 3]
                x_f_1 = 255. * (x_f_1 - np.min(x_f_1)) / \
                    (np.max(x_f_1) - np.min(x_f_1))
                x_f_2 = xs_f_2[i, :, :, 3]
                x_f_2 = 255. * (x_f_2 - np.min(x_f_2)) / \
                    (np.max(x_f_2) - np.min(x_f_2))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_palm_depth.png', x_palm.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_1_depth.png', x_f_1.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_2_depth.png', x_f_2.astype(int))
                x_palm = xs_palm[i, :, :, 4:7]
                x_palm = 255. * (x_palm - np.min(x_palm)) / \
                    (np.max(x_palm) - np.min(x_palm))
                x_f_1 = xs_f_1[i, :, :, 4:7]
                x_f_1 = 255. * (x_f_1 - np.min(x_f_1)) / \
                    (np.max(x_f_1) - np.min(x_f_1))
                x_f_2 = xs_f_2[i, :, :, 4:7]
                x_f_2 = 255. * (x_f_2 - np.min(x_f_2)) / \
                    (np.max(x_f_2) - np.min(x_f_2))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_palm_normal.png', x_palm.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_1_normal.png', x_f_1.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_2_normal.png', x_f_2.astype(int))
                x_palm = xs_palm[i, :, :, 7]
                x_palm = 255. * (x_palm - np.min(x_palm)) / \
                    (np.max(x_palm) - np.min(x_palm))
                x_f_1 = xs_f_1[i, :, :, 7]
                x_f_1 = 255. * (x_f_1 - np.min(x_f_1)) / \
                    (np.max(x_f_1) - np.min(x_f_1))
                x_f_2 = xs_f_2[i, :, :, 7]
                x_f_2 = 255. * (x_f_2 - np.min(x_f_2)) / \
                    (np.max(x_f_2) - np.min(x_f_2))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_palm_curv.png', x_palm.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_1_curv.png', x_f_1.astype(int))
                cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                            str(i) + '_f_2_curv.png', x_f_2.astype(int))

            k = self.dropout_prob
        else:
            if batch_idx is None:
                rand_indices = random.sample(
                    range(0, self.testing_samples_num), self.batch_size)
                ys = self.testing_labels[rand_indices]
                xs_palm = self.testing_samples_palm[rand_indices]
                xs_f_1 = self.testing_samples_f_1[rand_indices]
                xs_f_2 = self.testing_samples_f_2[rand_indices]
            else:
                ys = self.testing_labels[batch_idx * self.batch_size:
                                         min((batch_idx + 1) * self.batch_size, self.testing_samples_num)]
                xs_palm = self.testing_samples_palm[batch_idx * self.batch_size:
                                                    min((batch_idx + 1) * self.batch_size, self.testing_samples_num)]
                xs_f_1 = self.testing_samples_f_1[batch_idx * self.batch_size:
                                                  min((batch_idx + 1) * self.batch_size, self.testing_samples_num)]
                xs_f_2 = self.testing_samples_f_2[batch_idx * self.batch_size:
                                                  min((batch_idx + 1) * self.batch_size, self.testing_samples_num)]

            k = 1.0

        return {self.holder_labels: ys, self.holder_palm: xs_palm,
                self.holder_f_1: xs_f_1, self.holder_f_2: xs_f_2,
                self.keep_prob: k, self.learning_rate: l_rate}

    def train_test(self, train, read_batch=False):
        # Read data
        if read_batch:
            self.read_rgbd_data_batch(self.read_data_ratio)
        else:
            self.read_rgbd_data()
        # Create the variables.
        self.create_net_var()
        # Call the network building function.
        self.cost_function()

        self.var_palm = tf.get_variable(name='var_palm',
                                        shape=[self.batch_size, self.palm_patch_rows,
                                               self.palm_patch_cols, self.rgbd_channels],
                                        initializer=tf.constant_initializer(.0))
        self.var_f_1 = tf.get_variable(name='var_f_1',
                                       shape=[self.batch_size, self.finger_patch_rows,
                                              self.finger_patch_cols, self.rgbd_channels],
                                       initializer=tf.constant_initializer(.0))
        self.var_f_2 = tf.get_variable(name='var_f_2',
                                       shape=[self.batch_size, self.finger_patch_rows,
                                              self.finger_patch_cols, self.rgbd_channels],
                                       initializer=tf.constant_initializer(.0))

        self.assign_palm_var_op = self.var_palm.assign(self.holder_palm)
        self.assign_f_1_var_op = self.var_f_1.assign(self.holder_f_1)
        self.assign_f_2_var_op = self.var_f_2.assign(self.holder_f_2)

        # Initialize the variables
        init = tf.initialize_all_variables()
        saver = tf.train.Saver()
        tf.get_default_graph().finalize()
        logs_path = '/home/hashb/tmp/tf_logs/grasp_net_hand_cornell'
        if train:
            logs_path += '_train'
        else:
            logs_path += '_test'

        # op to write logs to Tensorboard
        # summary_writer = tf.train.SummaryWriter(
        #     logs_path, graph=tf.get_default_graph())

        start_time = time.time()
        train_costs = []
        train_pred_errors = []
        test_pred_errors = []
        learn_rate = 0.001
        #learn_rate = 0.0001
        # Launch the graph
        if train:
            with tf.Session() as sess:
                sess.run(init)
                # Training cycle
                for epoch in range(self.training_epochs):
                    if epoch != 100:
                        continue
                    if epoch % self.rate_dec_epochs == 0 and epoch != 0:
                        learn_rate *= 0.1
                    if read_batch and epoch % self.read_batch_epochs == 0 and epoch != 0:
                        self.read_rgbd_data_batch(self.read_data_ratio)

                    #[_, cost_output, pred_error_output, train_summary_output, f2vis_train_output, labels_train_output, pred_train_output] = \
                    #        sess.run([self.optimizer, self.cost, self.pred_error, self.train_summary, self.feature_to_vis,
                    # self.holder_labels, self.pred],
                    # feed_dict=self.feed_dict_func(True, learn_rate))

                    [_, cost_output, pred_error_output, train_summary_output, f2vis_train_output, labels_train_output, pred_train_output, _, _, _, xs_palm, xs_f_1, xs_f_2] = \
                        sess.run([self.optimizer, self.cost, self.pred_error, self.train_summary, self.feature_to_vis,
                                  self.holder_labels, self.pred, self.assign_palm_var_op, self.assign_f_1_var_op, self.assign_f_2_var_op, self.var_palm, self.var_f_1, self.var_f_2], feed_dict=self.feed_dict_func(True, learn_rate))

                    print 'labels_train_output:', labels_train_output
                    print 'pred_train_output', pred_train_output
                    # Write logs at every iteration
                    # summary_writer.add_summary(train_summary_output, epoch)

                    # Display logs per epoch step
                    train_costs.append(cost_output)
                    train_pred_errors.append(pred_error_output)

                    if epoch == 100:
                        for i in xrange(self.batch_size):
                            x_palm = xs_palm[i, :, :, :3]
                            x_f_1 = xs_f_1[i, :, :, :3]
                            x_f_2 = xs_f_2[i, :, :, :3]
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_palm_rgb_after.png', x_palm.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                                        str(i) + '_f_1_rgb_after.png', x_f_1.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                                        str(i) + '_f_2_rgb_after.png', x_f_2.astype(int))
                            x_palm = xs_palm[i, :, :, 3]
                            x_palm = 255. * \
                                (x_palm - np.min(x_palm)) / \
                                (np.max(x_palm) - np.min(x_palm))
                            x_f_1 = xs_f_1[i, :, :, 3]
                            x_f_1 = 255. * (x_f_1 - np.min(x_f_1)) / \
                                (np.max(x_f_1) - np.min(x_f_1))
                            x_f_2 = xs_f_2[i, :, :, 3]
                            x_f_2 = 255. * (x_f_2 - np.min(x_f_2)) / \
                                (np.max(x_f_2) - np.min(x_f_2))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_palm_depth_after.png', x_palm.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_f_1_depth_after.png', x_f_1.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_f_2_depth_after.png', x_f_2.astype(int))
                            x_palm = xs_palm[i, :, :, 4:7]
                            x_palm = 255. * \
                                (x_palm - np.min(x_palm)) / \
                                (np.max(x_palm) - np.min(x_palm))
                            x_f_1 = xs_f_1[i, :, :, 4:7]
                            x_f_1 = 255. * (x_f_1 - np.min(x_f_1)) / \
                                (np.max(x_f_1) - np.min(x_f_1))
                            x_f_2 = xs_f_2[i, :, :, 4:7]
                            x_f_2 = 255. * (x_f_2 - np.min(x_f_2)) / \
                                (np.max(x_f_2) - np.min(x_f_2))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_palm_normal_after.png', x_palm.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_f_1_normal_after.png', x_f_1.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_f_2_normal_after.png', x_f_2.astype(int))
                            x_palm = xs_palm[i, :, :, 7]
                            x_palm = 255. * \
                                (x_palm - np.min(x_palm)) / \
                                (np.max(x_palm) - np.min(x_palm))
                            x_f_1 = xs_f_1[i, :, :, 7]
                            x_f_1 = 255. * (x_f_1 - np.min(x_f_1)) / \
                                (np.max(x_f_1) - np.min(x_f_1))
                            x_f_2 = xs_f_2[i, :, :, 7]
                            x_f_2 = 255. * (x_f_2 - np.min(x_f_2)) / \
                                (np.max(x_f_2) - np.min(x_f_2))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' + str(i) +
                                        '_palm_curv_after.png', x_palm.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                                        str(i) + '_f_1_curv_after.png', x_f_1.astype(int))
                            cv2.imwrite('/home/hashb/tmp/tmp/' + 'sample_' +
                                        str(i) + '_f_2_curv_after.png', x_f_2.astype(int))

                    #[pred_output, y_output] = sess.run([pred, holder_labels], feed_dict=feed_dict_func(False))
                    #pred_error_test = np.mean(np.abs(y_output - pred_output))
                    [pred_error_test_output, pred_err_test_sum_output, f2vis_output_test, labels_test_output, pred_test_output] = sess.run(
                        [self.pred_error_test, self.pred_err_test_sum,
                            self.feature_to_vis, self.holder_labels, self.pred],
                        feed_dict=self.feed_dict_func(False))
                    print 'labels_test_output:', labels_test_output
                    print 'pred_test_output', pred_test_output
                    test_pred_errors.append(pred_error_test_output)
                    # summary_writer.add_summary(pred_err_test_sum_output, epoch)

                    if epoch % self.display_step == 0:
                        print 'epoch: ', epoch
                        print 'train_cost: ', cost_output
                        print 'pred_error_train: ', pred_error_output
                        # print 'train pred std: ',
                        # np.std(np.argmax(pred_train_output, 1))
                        print 'train pred std: ', np.std(pred_train_output[:, 0])
                        print 'pred_error_test: ', pred_error_test_output
                        # print 'test pred std: ',
                        # np.std(np.argmax(pred_test_output, 1))
                        print 'test pred std: ', np.std(pred_test_output[:, 0])

                        print 'fc1 ********************'
                        [w_palm_fc1, b_palm_fc1, w_f_1_fc1, b_f_1_fc1, w_f_2_fc1, b_f_2_fc1] = \
                            sess.run([self.weights_palm['fc1'], self.biases_palm['fc1'],
                                      self.weights_f_1[
                                          'fc1'], self.biases_f_1['fc1'],
                                      self.weights_f_2['fc1'], self.biases_f_2['fc1']])
                        # print w_palm_fc1, b_palm_fc1, w_f_1_fc1, b_f_1_fc1, w_f_2_fc1, b_f_2_fc1
                        # print np.array(w_palm_conv2).shape, np.array(b_palm_conv2).shape
                        # print np.array(w_f_1_conv2).shape, np.array(b_f_1_conv2).shape
                        # print np.array(w_f_2_conv2).shape,
                        # np.array(b_f_2_conv2).shape
                        print 'mean(abs) of weights and biases for palm, finger1 and finger 2'
                        print np.mean(np.abs(w_palm_fc1)), np.mean(np.abs(b_palm_fc1))
                        print np.mean(np.abs(w_f_1_fc1)), np.mean(np.abs(b_f_1_fc1))
                        print np.mean(np.abs(w_f_2_fc1)), np.mean(np.abs(b_f_2_fc1))
                        print 'std(abs) of weights and biases for palm, finger1 and finger 2'
                        print np.std(np.abs(w_palm_fc1)), np.std(np.abs(b_palm_fc1))
                        print np.std(np.abs(w_f_1_fc1)), np.std(np.abs(b_f_1_fc1))
                        print np.std(np.abs(w_f_2_fc1)), np.std(np.abs(b_f_2_fc1))

                    if epoch % (1000 * self.display_step) == 0:
                        tsne_vis.tsne_vis(f2vis_train_output, labels_train_output[:, 0],
                                          self.model_path + '/models/tsne/train_tsne_' + str(epoch) + '_gt.png')
                        tsne_vis.tsne_vis(f2vis_output_test, labels_test_output[:, 0],
                                          self.model_path + '/models/tsne/test_tsne_' + str(epoch) + '_gt.png')
                        tsne_vis.tsne_vis(f2vis_train_output, pred_train_output[:, 0],
                                          self.model_path + '/models/tsne/train_tsne_' + str(epoch) + '_pred.png')
                        tsne_vis.tsne_vis(f2vis_output_test, pred_test_output[:, 0],
                                          self.model_path + '/models/tsne/test_tsne_' + str(epoch) + '_pred.png')

                print("Optimization Finished!")
                saver.save(sess, self.model_path + '/models/cnn_hand.ckpt')

                fig, ax = plt.subplots(figsize=(8, 8))
                ax.plot(np.linspace(1, self.training_epochs,
                                    self.training_epochs), train_costs, 'r')
                fig.savefig(self.model_path + '/models/train_costs.png')
                plt.close(fig)
                fig, ax = plt.subplots(figsize=(8, 8))
                ax.plot(np.linspace(1, self.training_epochs,
                                    self.training_epochs), train_pred_errors, 'b')
                fig.savefig(self.model_path + '/models/train_pred_errors.png')
                plt.close(fig)
                fig, ax = plt.subplots(figsize=(8, 8))
                ax.plot(np.linspace(1, self.training_epochs,
                                    self.training_epochs), test_pred_errors, 'k')
                fig.savefig(self.model_path + '/models/test_pred_errors.png')
                plt.close(fig)
        else:
            with tf.Session() as sess:
                # Test model
                saver.restore(sess, self.model_path + '/models/cnn_hand.ckpt')
                pred_output = np.array([]).reshape(0, self.classes_num)
                f2vis_output = np.zeros(
                    [self.testing_samples_num, 32 * 3 + 32])
                for i in xrange(np.ceil(float(self.testing_samples_num) / self.batch_size).astype(int)):
                    [b_hand, w_hand, f2vis_batch_output, pred_batch_output] = sess.run(
                        [self.biases_hand['out'], self.weights_hand[
                            'out'], self.feature_to_vis, self.pred],
                        feed_dict=self.feed_dict_func(False, .0, i))
                    # print pred_batch_output.shape
                    # print 'bias_output: ', b_hand
                    # print 'weights_output: ', w_hand
                    # print 'pred_batch_output: ', pred_batch_output
                    pred_output = np.concatenate(
                        (pred_output, pred_batch_output))
                    # print f2vis_batch_output.shape
                    start_idx = i * self.batch_size
                    f2vis_output[start_idx: start_idx +
                                 len(f2vis_batch_output), :] = f2vis_batch_output
                #pred_output = pred_output[0:200]
                #self.testing_labels = self.testing_labels[0:200]
                print 'pred_output: ', pred_output
                print 'self.testing_labels: ', self.testing_labels
                # print 'np.argmax(self.testing_labels, 1):' , np.argmax(self.testing_labels, 1)
                # print np.argmax(self.testing_labels, 1).shape
                # print 'np.argmax(pred_output, 1):' , np.argmax(pred_output, 1)
                # print np.argmax(pred_output, 1).shape
                #pred_errors_all = np.abs(np.argmax(self.testing_labels, 1) - np.argmax(pred_output, 1))
                #avg_pred_error_test = np.mean(pred_errors_all)
                #print('avg_pred_error_test:', avg_pred_error_test)
                pred_output[pred_output > 0.5] = 1.
                pred_output[pred_output <= 0.5] = 0.
                print 'binary pred_output: ', pred_output
                print 'pred_output.shape: ', pred_output.shape
                print 'self.testing_labels.shape: ', self.testing_labels.shape
                pred_errors_all = np.abs(pred_output - self.testing_labels)
                avg_pred_error_test = np.mean(pred_errors_all)
                print('avg_pred_error_test:', avg_pred_error_test)

                print f2vis_output.shape, self.testing_labels.shape
                #tsne_vis.tsne_vis(f2vis_output, np.argmax(self.testing_labels, 1), self.model_path + '/models/tsne/test_tsne.png', True)
                #tsne_vis.tsne_vis(f2vis_output, np.argmax(pred_output, 1), self.model_path + '/models/tsne/test_tsne.png', True)
                tsne_vis.tsne_vis(f2vis_output, self.testing_labels[
                                  :, 0], self.model_path + '/models/tsne/test_tsne_gt.png', True)
                tsne_vis.tsne_vis(f2vis_output, pred_output[
                                  :, 0], self.model_path + '/models/tsne/test_tsne_pred.png', True)

                fig, ax = plt.subplots(figsize=(8, 8))
                ax.plot(np.linspace(1, self.testing_samples_num, self.testing_samples_num),
                        self.testing_labels, 'ro', label='gt labels')
                ax.plot(np.linspace(1, self.testing_samples_num,
                                    self.testing_samples_num), pred_output, 'bo', label='pred labels')
                ax.plot(np.linspace(1, self.testing_samples_num, self.testing_samples_num),
                        pred_errors_all, 'k*', label='pred errors')
    #             plt.legend(handles=[gt_plot, pred_plot, error_plot])
                legend = ax.legend(loc='upper right', shadow=True)
                frame = legend.get_frame()
                frame.set_facecolor('0.90')
                fig.savefig(self.model_path + '/models/labels_test.png')
                plt.close(fig)

        elapsed_time = time.time() - start_time
        print 'total train/testing elapsed_time: ', elapsed_time

    def create_net_var_inf(self):
        self.var_palm = tf.get_variable(name='var_palm',
                                        shape=[
                                            1, self.palm_patch_rows, self.palm_patch_cols, self.rgbd_channels],
                                        initializer=tf.constant_initializer(.0))
        self.var_f_1 = tf.get_variable(name='var_f_1',
                                       shape=[
                                           1, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels],
                                       initializer=tf.constant_initializer(.0))
        self.var_f_2 = tf.get_variable(name='var_f_2',
                                       shape=[
                                           1, self.finger_patch_rows, self.finger_patch_cols, self.rgbd_channels],
                                       initializer=tf.constant_initializer(.0))

    def construct_grad_graph_inf(self):
        self.assign_palm_var_op = self.var_palm.assign(self.holder_palm)
        self.assign_f_1_var_op = self.var_f_1.assign(self.holder_f_1)
        self.assign_f_2_var_op = self.var_f_2.assign(self.holder_f_2)

        with tf.name_scope('Pred'):
            (fc_concat_hand, fc1_hand, logits) = self.conv_net_hand(self.var_palm, self.var_f_1, self.var_f_2,
                                                                    self.weights_palm, self.biases_palm, self.weights_f_1, self.biases_f_1,
                                                                    self.weights_f_2, self.biases_f_2, self.weights_hand, self.biases_hand)
            #self.pred = tf.nn.softmax(logits)
            self.pred = tf.nn.sigmoid(logits)

        # with tf.name_scope('Pred'):
        #    (fc_concat_hand, fc1_hand, logits) = self.conv_net_hand(self.holder_palm, self.holder_f_1, self.holder_f_2,
        #                          self.weights_palm, self.biases_palm, self.weights_f_1, self.biases_f_1,
        #                          self.weights_f_2, self.biases_f_2, self.weights_hand, self.biases_hand)
        #    self.pred = tf.nn.softmax(logits)

        with tf.name_scope('GD'):
            self.optimizer = tf.train.GradientDescentOptimizer(
                learning_rate=0.)

        with tf.name_scope('RGBD_GRAD'):
            self.rgbd_gradients = self.optimizer.compute_gradients(logits[0, 0],
                                                                   var_list=[self.var_palm, self.var_f_1, self.var_f_2])
            #self.suc_prob = tf.add(logits[0, 1], -logits[0, 0])
            # self.rgbd_gradients = self.optimizer.compute_gradients(self.suc_prob,
            #        var_list=[self.var_palm, self.var_f_1, self.var_f_2])

    def init_net_inf(self):
        # Create the network variables.
        self.create_net_var()

        saver = tf.train.Saver()
        self.sess_inf = tf.Session()
        # Initialize the variables
        self.init = tf.global_variables_initializer()
        self.sess_inf.run(self.init)
        # Restore trained model
        saver.restore(self.sess_inf, self.model_path + '/models/cnn_hand.ckpt')
        #saver.restore(self.sess_inf, self.model_path + '/models_raw/cnn_hand.ckpt')

        # Create variables for rgbd patches
        self.create_net_var_inf()
        # Call the network building function.
        self.construct_grad_graph_inf()

        tf.get_default_graph().finalize()
        logs_path = '/home/hashb/tmp/tf_logs/grasp_net_hand_cornell_inf'
        # op to write logs to Tensorboard
        # summary_writer = tf.train.SummaryWriter(
        #     logs_path, graph=tf.get_default_graph())

    def get_rgbd_gradients(self, palm_patch, finger_1_patch, finger_2_patch):
        self.sess_inf.run([self.assign_palm_var_op, self.assign_f_1_var_op, self.assign_f_2_var_op],
                          feed_dict={self.holder_palm: np.array([palm_patch]),
                                     self.holder_f_1: np.array([finger_1_patch]),
                                     self.holder_f_2: np.array([finger_2_patch])})

        [gradients, pred_out] = self.sess_inf.run(
            [self.rgbd_gradients, self.pred], feed_dict={self.keep_prob: 1.})
        #[gradients, suc_prob_out] = self.sess_inf.run([self.rgbd_gradients, self.suc_prob], feed_dict={self.keep_prob:1.})
        #[suc_prob_out] = self.sess_inf.run([self.suc_prob], feed_dict={self.keep_prob:1., self.holder_palm:np.array([palm_patch]), self.holder_f_1:np.array([finger_1_patch]), self.holder_f_2:np.array([finger_2_patch])})
        #[suc_prob_out] = self.sess_inf.run([self.suc_prob], feed_dict={self.keep_prob:1., self.holder_palm:np.ones((1,200,200,8)), self.holder_f_1:np.ones((1,100,100,8)), self.holder_f_2:np.ones((1,100,100,8))})
        # print np.array([palm_patch]).shape
        # print 'suc_prob_out:', suc_prob_out
        # print 'pred_out:', pred_out
        return gradients[0][0][0], gradients[1][0][0], gradients[2][0][0], pred_out[0, 0]
        # return 0,0,0,suc_prob_out


def main():
   # data_path = '/home/hashb/tmp/tars_HDD/cornell_grasp_data/'
   # patch_file = h5py.File(data_path + 'h5_data/patch.h5', 'r')
   # #print patch_file.keys()
   # labels = patch_file['labels']
   # print labels.shape, labels[0:10], labels[-11:]
   # grasps_num = labels.shape[0]
   # rgbd = patch_file['palm_' + str(0)]#str(grasps_num-1)]
   # print rgbd.shape
   # path = '../patches'
   # gp.plot_rgbd(rgbd, path)
   # patch_file.close()

    grasp_rgbd_net = GraspRgbdNet()
    grasp_rgbd_net.train_test(True, read_batch=True)
    # grasp_rgbd_net.train_test(False)

    # grasp_rgbd_net.init_net_inf()
    # print 'conv1 ********************'
    #[w_palm_conv1, b_palm_conv1, w_f_1_conv1, b_f_1_conv1, w_f_2_conv1, b_f_2_conv1] = \
    #        grasp_rgbd_net.sess_inf.run([grasp_rgbd_net.weights_palm['conv1'], grasp_rgbd_net.biases_palm['conv1'],
    #        grasp_rgbd_net.weights_f_1['conv1'], grasp_rgbd_net.biases_f_1['conv1'],
    #        grasp_rgbd_net.weights_f_2['conv1'], grasp_rgbd_net.biases_f_2['conv1']])
    # print w_palm_conv1, b_palm_conv1, w_f_1_conv1, b_f_1_conv1, w_f_2_conv1, b_f_2_conv1
    # print np.array(w_palm_conv1).shape, np.array(b_palm_conv1).shape
    # print np.array(w_f_1_conv1).shape, np.array(b_f_1_conv1).shape
    # print np.array(w_f_2_conv1).shape, np.array(b_f_2_conv1).shape
    # print 'mean(abs) of weights and biases for palm, finger1 and finger 2'
    # print np.mean(np.abs(w_palm_conv1)), np.mean(np.abs(b_palm_conv1))
    # print np.mean(np.abs(w_f_1_conv1)), np.mean(np.abs(b_f_1_conv1))
    # print np.mean(np.abs(w_f_2_conv1)), np.mean(np.abs(b_f_2_conv1))
    # print 'std(abs) of weights and biases for palm, finger1 and finger 2'
    # print np.std(np.abs(w_palm_conv1)), np.std(np.abs(b_palm_conv1))
    # print np.std(np.abs(w_f_1_conv1)), np.std(np.abs(b_f_1_conv1))
    # print np.std(np.abs(w_f_2_conv1)), np.std(np.abs(b_f_2_conv1))

    # print 'conv2 ********************'
    #[w_palm_conv2, b_palm_conv2, w_f_1_conv2, b_f_1_conv2, w_f_2_conv2, b_f_2_conv2] = \
    #        grasp_rgbd_net.sess_inf.run([grasp_rgbd_net.weights_palm['conv2'], grasp_rgbd_net.biases_palm['conv2'],
    #        grasp_rgbd_net.weights_f_1['conv2'], grasp_rgbd_net.biases_f_1['conv2'],
    #        grasp_rgbd_net.weights_f_2['conv2'], grasp_rgbd_net.biases_f_2['conv2']])
    # print w_palm_conv2, b_palm_conv2, w_f_1_conv2, b_f_1_conv2, w_f_2_conv2, b_f_2_conv2
    # print np.array(w_palm_conv2).shape, np.array(b_palm_conv2).shape
    # print np.array(w_f_1_conv2).shape, np.array(b_f_1_conv2).shape
    # print np.array(w_f_2_conv2).shape, np.array(b_f_2_conv2).shape
    # print 'mean(abs) of weights and biases for palm, finger1 and finger 2'
    # print np.mean(np.abs(w_palm_conv2)), np.mean(np.abs(b_palm_conv2))
    # print np.mean(np.abs(w_f_1_conv2)), np.mean(np.abs(b_f_1_conv2))
    # print np.mean(np.abs(w_f_2_conv2)), np.mean(np.abs(b_f_2_conv2))
    # print 'std(abs) of weights and biases for palm, finger1 and finger 2'
    # print np.std(np.abs(w_palm_conv2)), np.std(np.abs(b_palm_conv2))
    # print np.std(np.abs(w_f_1_conv2)), np.std(np.abs(b_f_1_conv2))
    # print np.std(np.abs(w_f_2_conv2)), np.std(np.abs(b_f_2_conv2))
    #
    # print 'fc1 ********************'
    #[w_palm_fc1, b_palm_fc1, w_f_1_fc1, b_f_1_fc1, w_f_2_fc1, b_f_2_fc1] = \
    #        grasp_rgbd_net.sess_inf.run([grasp_rgbd_net.weights_palm['fc1'], grasp_rgbd_net.biases_palm['fc1'],
    #        grasp_rgbd_net.weights_f_1['fc1'], grasp_rgbd_net.biases_f_1['fc1'],
    #        grasp_rgbd_net.weights_f_2['fc1'], grasp_rgbd_net.biases_f_2['fc1']])
    # print w_palm_fc1, b_palm_fc1, w_f_1_fc1, b_f_1_fc1, w_f_2_fc1, b_f_2_fc1
    # print np.array(w_palm_conv2).shape, np.array(b_palm_conv2).shape
    # print np.array(w_f_1_conv2).shape, np.array(b_f_1_conv2).shape
    # print np.array(w_f_2_conv2).shape, np.array(b_f_2_conv2).shape
    # print 'mean(abs) of weights and biases for palm, finger1 and finger 2'
    # print np.mean(np.abs(w_palm_fc1)), np.mean(np.abs(b_palm_fc1))
    # print np.mean(np.abs(w_f_1_fc1)), np.mean(np.abs(b_f_1_fc1))
    # print np.mean(np.abs(w_f_2_fc1)), np.mean(np.abs(b_f_2_fc1))
    # print 'std(abs) of weights and biases for palm, finger1 and finger 2'
    # print np.std(np.abs(w_palm_fc1)), np.std(np.abs(b_palm_fc1))
    # print np.std(np.abs(w_f_1_fc1)), np.std(np.abs(b_f_1_fc1))
    # print np.std(np.abs(w_f_2_fc1)), np.std(np.abs(b_f_2_fc1))

    # print 'fc1 hand ********************'
    #[w_hand_fc1, b_hand_fc1] = \
    #        grasp_rgbd_net.sess_inf.run([grasp_rgbd_net.weights_hand['fc1'], grasp_rgbd_net.biases_hand['fc1']])
    # print w_hand_fc1, b_hand_fc1
    # print np.array(w_hand_fc1).shape, np.array(b_hand_fc1).shape
    # print 'mean(abs) of weights and biases for hand'
    # print np.mean(np.abs(w_hand_fc1)), np.mean(np.abs(b_hand_fc1))
    # print 'std(abs) of weights and biases for hand'
    # print np.std(np.abs(w_hand_fc1)), np.std(np.abs(b_hand_fc1))

    # print 'out hand ********************'
    #[w_hand_out, b_hand_out] = \
    #        grasp_rgbd_net.sess_inf.run([grasp_rgbd_net.weights_hand['out'], grasp_rgbd_net.biases_hand['out']])
    # print w_hand_out, b_hand_out
    # print np.array(w_hand_fc1).shape, np.array(b_hand_fc1).shape
    # print 'mean(abs) of weights and biases for hand'
    # print np.mean(np.abs(w_hand_out)), np.mean(np.abs(b_hand_out))
    # print 'std(abs) of weights and biases for hand'
    # print np.std(np.abs(w_hand_out)), np.std(np.abs(b_hand_out))


if __name__ == '__main__':
    main()
