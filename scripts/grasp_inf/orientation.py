import numpy as np
import math
from math import cos, sin

def unit_vec(vec):
    return vec/np.linalg.norm(vec)

def rot_around_x(th):
    cTh = cos(th)
    sTh = sin(th)

    return np.array([[1, 0, 0], [0, cTh, -sTh], [0, sTh, cTh]])

def rot_around_y(th):
    cTh = cos(th)
    sTh = sin(th)

    return np.array([[cTh, 0, sTh], [0, 1, 0], [-sTh, 0, cTh]])

def rot_around_z(th):
    cTh = cos(th)
    sTh = sin(th)

    return np.array([[cTh, -sTh, 0], [sTh, cTh, 0], [0, 0, 1]])

def spherical_rot(az, el):
    return rot_around_y(az)*rot_around_x(el)

def rot_to_align_z(new_z):
    print "newz"
    print new_z[0]
    print new_z[2]
    az = math.atan2(new_z[0], new_z[2])
    el = -math.atan2(new_z[1], np.linalg.norm([new_z[0], new_z[2]]))

    return spherical_rot(az, el)

def get_xy_slope_on_plane(N):
    return -(N[0]/(N[1]*1.0))

def rot_to_level_y_xy_plane(R):
    xy_slope = get_xy_slope_on_plane(R[:,2])

    if np.abs(xy_slope) < float('inf'):
        PXY = np.array([[1, xy_slope, 0]])
    elif np.isnan(xy_slope):
        PXY = np.array([[0, 1, 0]])
    else:
        PXY = np.array([[0, np.sign(xy_slope), 0]])

    PXY = R.T * PXY

    print PXY

    roll_angle = math.atan2(PXY[1], PXY[0]) + np.pi/2

    return rot_around_z(roll_angle)

def align_to_ax_and_level(v):
    R = rot_to_align_z(v)
    return R*rot_to_level_y_xy_plane(R)

def get_gripper_matrix(ax, ang):
    return align_to_ax_and_level(ax)*rot_around_z(ang)

def get_quaternion(R):
    r, c = R.shape

    if (r != 3) or (c != 3):
        print "R should be a rotation matrix 3x3"
        exit(0)

    Rxx = R[0, 0]
    Rxy = R[0, 1]
    Rxz = R[0, 2]

    Ryx = R[1, 0]
    Ryy = R[1, 1]
    Ryz = R[1, 2]

    Rzx = R[2, 0]
    Rzy = R[2, 1]
    Rzz = R[2, 2]

    w = np.sqrt(np.trace(R) + 1) / 2;

    if w.imag > 0:
        w = 0

    x = np.sqrt(1 + Rxx - Ryy - Rzz) / 2
    y = np.sqrt(1 + Ryy - Rxx - Rzz) / 2
    z = np.sqrt(1 + Rzz - Ryy - Rxx) / 2

    idx = np.argmax(np.array([w, x, y, z]))

    if idx == 0:
        x = ( Rzy - Ryz ) / (4*w)
        y = ( Rxz - Rzx ) / (4*w)
        z = ( Ryx - Rxy ) / (4*w)

    if idx == 1:
        w = ( Rzy - Ryz ) / (4*x)
        y = ( Rxy + Ryx ) / (4*x)
        z = ( Rzx + Rxz ) / (4*x)

    if idx == 2:
        w = ( Rxz - Rzx ) / (4*y)
        x = ( Rxy + Ryx ) / (4*y)
        z = ( Ryz + Rzy ) / (4*y)

    if idx == 3:
        w = ( Ryx - Rxy ) / (4*z)
        x = ( Rzx + Rxz ) / (4*z)
        y = ( Ryz + Rzy ) / (4*z)

    return np.array([x, y, z, w])


def get_orientation(gP1, gP2, obj_normal):
    # convert normal to a unit vector
    ax = unit_vec(obj_normal)

    # find the grip difference
    grip_diff = gP2 - gP1
    # find the rotation angle
    rot_angle = math.atan2(grip_diff[1], grip_diff[0])

    or_rot = get_gripper_matrix(ax, rot_angle)
    or_quat = get_quaternion(or_rot)

    return or_quat
