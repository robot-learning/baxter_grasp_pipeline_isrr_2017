#!/usr/bin/env python

import os
import math

import orientation
import run_rgbd_inf as inf
import gen_rgbd_patches as gp
from grasp_rgbd_inf import GraspRgbdInf

import rospy
import roslib
roslib.load_manifest('baxter_pipeline')
import tf
from cv_bridge import CvBridge, CvBridgeError
import cv2

from baxter_pipeline.srv import *
from tabletop_obj_segmentation.srv import *

import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import CameraInfo, Image, PointCloud2
from geometry_msgs.msg import PoseStamped
import ctypes
import struct
import numpy as np

# for rviz
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray


_DEBUG = False
_TESTING = False  # To pass dummy values


def sqr_dist(x, y):
    '''
    Get the squared distance between two vectors
    '''
    xx = x - y
    xx = np.multiply(xx, xx)
    xx = np.sum(xx)
    return xx


def find_nearest_neighbor(sample_point, cloud):
    '''
    Find the nearest neigbor between sample_point and the points in cloud.
    Uses the squared Euclidean distance.
    '''
    cloud = np.array(list(cloud))[:,:3].reshape((480, 640, 3))
    point_dist = []
    # print cloud.shape
    min_dist = float('inf')
    min_pt = None
    min_idx = 0
    cloud_shape = cloud.shape
    for x in range(cloud_shape[0]):
        for y in range(cloud_shape[1]):
            pt = cloud[x][y]
            # print pt
            pt_dist = sqr_dist(np.array(pt), sample_point)
            if np.isnan(pt_dist):
                continue
            min_pt = np.array(pt[:])
            min_dist = pt_dist
            min_x = x
            min_y = y
            point_dist.append([min_pt, [min_x, min_y], min_dist])
    sorted_point_dist = sorted(point_dist, key=lambda x: x[2])
    return sorted_point_dist[0]


def get_orientation(obj_normal, finger_vector, finger_loc_img=None):
    y_rand = finger_vector/ np.linalg.norm(finger_vector)
    z_axis = -np.sign(obj_normal[2])*obj_normal / np.linalg.norm(obj_normal)
    y_onto_z = y_rand.dot(z_axis) * z_axis
    y_axis = y_rand - y_onto_z
    # Normalize to unit length
    y_axis /= np.linalg.norm(y_axis)
    # Find the third orthonormal component and build the rotation matrix
    x_axis = np.cross(y_axis, z_axis)
    x_axis /= np.linalg.norm(x_axis)
    rot_matrix = np.matrix([x_axis, y_axis, z_axis]).T

    # Compute quaternion from rpy
    trans_matrix = np.matrix(np.zeros((4, 4)))
    trans_matrix[:3, :3] = rot_matrix
    trans_matrix[3, 3] = 1.
    quaternion = tf.transformations.quaternion_from_matrix(trans_matrix)
    if _DEBUG: rospy.loginfo('Grasp orientation = ' + str(quaternion))
    return quaternion

def publish_points(points_stamped, publisher_name='/publish_box_points',
        color=(1., 0., 0.)):
    g_pose_pub=rospy.Publisher(publisher_name, MarkerArray, queue_size=10)
    markerArray = MarkerArray()
    for i, pnt in enumerate(points_stamped):
        marker = Marker()
        marker.header.frame_id = pnt.header.frame_id
        marker.type = marker.SPHERE
        marker.action = marker.ADD
        marker.scale.x = 0.03
        marker.scale.y = 0.03
        marker.scale.z = 0.03
        marker.pose.orientation.w = 1.0
        marker.color.a = 1.0
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        marker.pose.position.x = pnt.pose.position.x
        marker.pose.position.y = pnt.pose.position.y
        marker.pose.position.z = pnt.pose.position.z
        marker.id = i
        markerArray.markers.append(marker)
    g_pose_pub.publish(markerArray)

def proj_point_from_camera_to_img(point):
    """
    Project one point from 3d camera space to 2d image space.
    """
    #homo_point = self.proj_mat * point
    camera_info_k_mat = np.array([570.3422241210938, 0.0, 314.5, 0.0, 570.3422241210938, 235.5, 0.0, 0.0, 1.0])  # Uncalibrated
    # camera_info_k_mat = np.array([532.8342934318664, 0.0, 312.8613688487357, 0.0, 530.8973312692033, 237.622760240668, 0.0, 0.0, 1.0])  # Calibrated
    proj_mat = np.reshape(camera_info_k_mat, (3, 3))
    homo_point = np.matmul(proj_mat, point)
    # Convert projection matrix parameters from mm to meters.
    homo_point *= 0.001
    proj_x = int(homo_point[0] / homo_point[2])
    proj_y = int(homo_point[1] / homo_point[2])
    return np.array([proj_x, proj_y])


def proj_point_from_img_to_camera(point, height):
    """
    project 2d point to 3d
    """
    camera_info_k_mat = np.array([570.3422241210938, 0.0, 314.5, 0.0, 570.3422241210938, 235.5, 0.0, 0.0, 1.0])  # Uncalibrated
    proj_mat = np.reshape(camera_info_k_mat, (3, 3))

    proj_mat_inv = np.linalg.inv(proj_mat)

    pixel_uvd = np.array([[point[0], point[1], height]]).T

    point_xyz = np.matmul(proj_mat_inv, pixel_uvd)
    # print point_xyz

    return point_xyz.T[0]


def rectangle2pose(rect, response):
    # TODO: Check for nan values and dont send them as pose
    tl = tf.TransformListener()

    points_xyz = list(pc2.read_points(
        response.obj.transformed_cloud, field_names=['x', 'y', 'z']))  # Base frame

    frame = 'base'
    camera_frame = response.obj.p_cloud.header.frame_id

    gripper_pose = PoseStamped()
    gripper_pose.header.frame_id = frame
    gripper_pose.header.stamp = rospy.Time.now()

    # Rectangle center and also few points around it
    palm_loc = tuple(np.mean(rect, 0).astype(int))
    palm_loc_idx = palm_loc[0] + 640 * palm_loc[1]
    palm_loc_idx1 = (palm_loc[0]) + 640 * (palm_loc[1] +1)
    palm_loc_idx2 = (palm_loc[0]+1) + 640 * (palm_loc[1] +0)
    palm_loc_idx3 = (palm_loc[0]+1) + 640 * (palm_loc[1] +1)
    palm_loc_idx4 = (palm_loc[0]+2) + 640 * (palm_loc[1] +1)

    palm_goal_pt = np.array(points_xyz[palm_loc_idx])  # Base frame center of rectangle

    # find the finger centers in image frame
    finger_1_loc = np.array(np.mean(rect[0:2, :], 0).astype(int))
    finger_2_loc = np.array(np.mean(rect[2:4, :], 0).astype(int))

    # print "#"*20
    # print "rect", rect
    # print "finger_1_loc", finger_1_loc
    # print "finger_2_loc", finger_2_loc
    # print "palm_loc", palm_loc

    try:
        slope_uv = (finger_2_loc[1] - finger_1_loc[1])/(finger_2_loc[0] - finger_1_loc[0])
        rospy.loginfo(finger_1_loc)
        rospy.loginfo(finger_2_loc)
        rospy.loginfo(slope_uv)
    except:
        slope_uv = 0
    
    if np.isnan(slope_uv):
        if abs(finger_2_loc[0] - finger_1_loc[0]) < 10**-6:
            x1 = palm_goal_pt[0]
            y1 = palm_goal_pt[1] + 2

            x2 = palm_goal_pt[0]
            y2 = palm_goal_pt[1] - 2
        else:
            x1 = palm_goal_pt[0] + 2
            y1 = palm_goal_pt[1]

            x2 = palm_goal_pt[0] - 2
            y2 = palm_goal_pt[1]
    else:
        x1 = palm_goal_pt[0] + 2
        y1 = slope_uv*2 + palm_goal_pt[1]
        # finger_1_pt_idx = row1 + 640*col1

        x2 = palm_goal_pt[0] - 2
        y2 = slope_uv*-2 + palm_goal_pt[1]
        # finger_2_pt_idx = row2 + 640*col2

    if _DEBUG:
        print "slope", slope_uv
        print "x1, y1 {}, {}".format(x1, y1)
        print "x2, y2 {}, {}".format(x2, y2)
        print '#'*20


    # finger_diff = finger_2_loc - finger_1_loc
    # theta = math.atan2(finger_diff[1], -finger_diff[0]) - np.pi/4

    finger_1_pt = np.array([x1, y1, palm_goal_pt[2]])
    # finger_1_pt[2] = 0
    finger_2_pt = np.array([x2, y2, palm_goal_pt[2]])
    # finger_1_pt[2] = 0
    # print "finger_1_pt", finger_1_pt
    # print "finger_2_pt", finger_2_pt
    # finger_1_pt = proj_point_from_img_to_camera(finger_1_loc, palm_goal_pt[2])
    # finger_2_pt = proj_point_from_img_to_camera(finger_2_loc, palm_goal_pt[2])
    if _DEBUG:
        f0 = PoseStamped()
        f0.header.frame_id = frame
        f0.header.stamp = rospy.Time.now()
        f0.pose.position.x = palm_goal_pt[0]
        f0.pose.position.y = palm_goal_pt[1]
        f0.pose.position.z = palm_goal_pt[2]

        f1 = PoseStamped()
        f1.header.frame_id = frame
        f1.header.stamp = rospy.Time.now()
        f1.pose.position.x = finger_1_pt[0]
        f1.pose.position.y = finger_1_pt[1]
        f1.pose.position.z = finger_1_pt[2]

        f2 = PoseStamped()
        f2.header.frame_id = frame
        f2.header.stamp = rospy.Time.now()
        f2.pose.position.x = finger_2_pt[0]
        f2.pose.position.y = finger_2_pt[1]
        f2.pose.position.z = finger_2_pt[2]

        publish_points([f0, f1, f2])

    point_normals = list(pc2.read_points(response.obj.normals, field_names=['normal_x',
                                                                            'normal_y',
                                                                            'normal_z']))  # base frame

    obj_normal = (np.array(point_normals[palm_loc_idx]) + \
                    np.array(point_normals[palm_loc_idx1]) + \
                    np.array(point_normals[palm_loc_idx2]) + \
                    np.array(point_normals[palm_loc_idx3]) + \
                    np.array(point_normals[palm_loc_idx4]))/5
    # make it unit vector
    obj_normal = obj_normal/np.linalg.norm(obj_normal)

    if abs(obj_normal[2] > 0.65):
        obj_normal = np.array([0, 0, np.sign(obj_normal[2])])

    # print "object normal unit vector: ", obj_normal
    # print "goal before adding offset: ", palm_goal_pt
    gripper_position_offset = 0.20  # 0.07 meters
    gripper_position = palm_goal_pt + (gripper_position_offset * np.sign(obj_normal[2]) * obj_normal)
    # print "goal after adding offset: ", gripper_position

    gripper_pose.pose.position.x = gripper_position[0]
    gripper_pose.pose.position.y = gripper_position[1]
    gripper_pose.pose.position.z = gripper_position[2]

    finger_vector = (finger_2_pt - finger_1_pt) / \
        np.linalg.norm((finger_2_pt - finger_1_pt))

    gripper_orientation = get_orientation(
        obj_normal, finger_vector, finger_loc_img=[finger_1_loc, finger_2_loc])
    # gripper_orientation = orientation.get_orientation(finger_1_loc,
    #                                                   finger_2_loc,
    #                                                   obj_normal)

    gripper_pose.pose.orientation.x = gripper_orientation[0]
    gripper_pose.pose.orientation.y = gripper_orientation[1]
    gripper_pose.pose.orientation.z = gripper_orientation[2]
    gripper_pose.pose.orientation.w = gripper_orientation[3]

    # return object location
    obj_location = PoseStamped()
    obj_location.header.frame_id = frame
    obj_location.header.stamp = rospy.Time.now()

    obj_location.pose.position.x = palm_goal_pt[0]
    obj_location.pose.position.y = palm_goal_pt[1]
    obj_location.pose.position.z = palm_goal_pt[2]

    obj_location.pose.orientation.x = gripper_orientation[0]
    obj_location.pose.orientation.y = gripper_orientation[1]
    obj_location.pose.orientation.z = gripper_orientation[2]
    obj_location.pose.orientation.w = gripper_orientation[3]


    return gripper_pose, obj_normal, obj_location


def process_rgbd_data(cloud, normals, sensor_img):
    gen1 = pc2.read_points(cloud,  skip_nans=False)
    gen2 = pc2.read_points(normals,  skip_nans=False)
    int_data1 = list(gen1)
    int_data2 = list(gen2)

    bridge = CvBridge()
    bgr = bridge.imgmsg_to_cv2(sensor_img, 'bgr8')
    n_rows = bgr.shape[0]
    n_cols = bgr.shape[1]
    n_channels = 8
    rgbd = np.zeros((n_rows, n_cols, n_channels))
    rgbd[:, :, :3] = bgr[:, :, :]
    for _i in range(len(int_data1)):
        x = int_data1[_i]
        y = int_data2[_i]

        row = _i / n_cols
        col = _i % n_cols

        rgbd[row, col, 3:] = np.array([x[2], y[0], y[1], y[2], y[3]])

    rgbd = np.nan_to_num(rgbd)
    # cv2.imshow('img0-color',rgbd[:,:,2::-1]/255.)

    # depth_max = np.nanmax(rgbd[:,:,3])
    # print 'depth max = ', depth_max
    # cv2.imshow('img3-d',rgbd[:,:,3]/depth_max)
    # cv2.imshow('img5-xyz-norms',rgbd[:,:,5:])
    # cv2.waitKey(0)
    # exit()

    return rgbd


def get_normals(cloud):
    rospy.wait_for_service('get_normals')
    try:
        getNormals = rospy.ServiceProxy('get_normals', GetNormals)
        req = GetNormalsRequest()
        req.input_cloud = cloud
        res = getNormals(req)
    except Exception as e:
        print e
        raise e
    else:
        return res

def get_segment():
    rospy.wait_for_service('object_segmenter')
    try:
        get_segment = rospy.ServiceProxy('object_segmenter', SegmentGraspObject)
        req = SegmentGraspObjectRequest()
        res = get_segment(req)
    except Exception as e:
        print e
        raise e
    else:
        return res


def get_rectangle_from_inference(rgbd, configs):
    grasp_inf = GraspRgbdInf()

    # Using configs
    # configs = inf.get_initial_configs(fg, bg)
    inference_data = []
    for i, _conf in enumerate(configs):
        conf_path = '/home/hashb/tmp/plots/init' + str(i)
        if not os.path.exists(conf_path):
            os.makedirs(conf_path)
        _conf_rect = gp.get_rect_from_config(_conf)
        gp.plot_rgbd(rgbd.copy(), conf_path, _conf_rect)
    if 'n' == raw_input('Check the initialization and let me know if you want to continue [y/n]> '):
        # return _conf_rect
        exit(0)
    print '#####################################'
    print '######## inference starting  ########'
    for i, config in enumerate(configs):
        config_opt, suc_prob, suc_prob_init = \
            grasp_inf.grad_descent_inf(
                rgbd.copy(), config)
                # rgbd.copy(), config.copy())
        print '######## Config {} Completed  ########'.format(i+1)
        inference_data.append((config_opt, suc_prob, suc_prob_init))

    suc_prob_vals = zip(*inference_data)
    best_config, best_suc_prob, best_suc_prob_init = inference_data[
        suc_prob_vals[1].index(max(suc_prob_vals[1]))]

    # best_config = config

    print '######## Inference Completed ########'
    print '#####################################'
    config_opt_rect = gp.get_rect_from_config(best_config)

    # config_opt_rect = gp.get_rect_from_config(config_opt)

    # print 'config_opt: ', config_opt
    # print 'config_opt_rect: ', config_opt_rect

    gp.plot_rgbd(rgbd.copy(), '/home/hashb/tmp/plots/final', config_opt_rect)
    
    inf_data = open('/home/hashb/tmp/plots/inf_data.txt', 'w')
    inf_data.write(str(suc_prob_vals))
    inf_data.close()

    # if _DEBUG:
    #     for c, s, s_i in inference_data:
    #         plot_path = '/home/hashb/tmp/plots/final_' + str(s)
    #         c_rect = gp.get_rect_from_config(c)
    #         if not os.path.exists(plot_path):
    #             os.makedirs(plot_path)
    #         gp.plot_rgbd(rgbd.copy(), plot_path, c_rect)

    return config_opt_rect


def image_callback(msg):
    print('Received an image!')
    try:
        # Convert your ROS Image message to OpenCV2
        bridge = CvBridge()
        cv2_img = bridge.imgmsg_to_cv2(msg, 'bgr8')
        cv2.imwrite('/home/hashb/tmp/foregrounds/rgb.jpg', cv2_img)
    except CvBridgeError, e:
        print(e)
    else:
        # Save your OpenCV2 image as a jpeg
        print 'Wrote foreground image to disk'


class SelectPixel(object):
    """docstring for SelectPixel"""
    def __init__(self):
        self.x = None
        self.y = None

    def select_uv_from_image_callback(self,event,x,y,flags,param):
        # print x, y
        if event == cv2.EVENT_LBUTTONDBLCLK:
            print "got pixels"
            print x, y
            self.x = x
            self.y = y

    def select_uv_from_image(self, img):
        bridge = CvBridge()
        bgr = bridge.imgmsg_to_cv2(img, 'bgr8')

        cv2.namedWindow("select_pixel")
        cv2.setMouseCallback("select_pixel", self.select_uv_from_image_callback)
        while(1):
            cv2.imshow("select_pixel", bgr)
            if cv2.waitKey(10)%0x100 == 27: break

        cv2.destroyWindow("select_pixel")

        # rospy.sleep(20)


def grasp_callback_table_segment(request):
    response = GripperGraspInfResponse()
    # get segment and pointcloud
    # using pointcloud get normals
    # process data

    raw_input("press enter to take background image> ")
    background = rospy.wait_for_message('/camera/rgb/image_raw', Image)

    point_cloud = rospy.wait_for_message('/camera/depth_registered/points', PointCloud2)

    # obj_pose = segment_data.obj.pose  # pointcloud frame

    normals_and_rgbd = get_normals(point_cloud)
    # print get_normals(point_cloud)
    # exit(0)

    rgbd = process_rgbd_data(normals_and_rgbd.obj.p_cloud, normals_and_rgbd.obj.normals, normals_and_rgbd.full_image)
    points_xyz = list(pc2.read_points(
        normals_and_rgbd.obj.transformed_cloud, field_names=['x', 'y', 'z']))
    np.save('/home/hashb/tmp/plots/rgbd.npy', rgbd)
    np.save('/home/hashb/tmp/plots/pointcloud.npy', points_xyz)

    # convert point from obj_pose to image coordiante
    # point = np.array([obj_pose.position.x, obj_pose.position.y, obj_pose.position.z])  # point cloud frame

    # image_point = proj_point_from_camera_to_img(point)  # image frame

    # print "image point from segmentation ", image_point

    # nearest_point, nearest_point_idx, nearest_point_dist = tuple(find_nearest_neighbor(point, pc2.read_points(point_cloud)))

    # print "nearest point idx", nearest_point_idx
    # print "nearest point", nearest_point
    # print "nearest point dist", nearest_point_dist
    # image_point = nearest_point_idx

    # Manually select point
    # NOTE: This is only for testing
    # Comment the next 3 lines when done testing
    pixel_selector = SelectPixel()
    pixel_selector.select_uv_from_image(normals_and_rgbd.full_image)
    image_point = [pixel_selector.x, pixel_selector.y]
    del pixel_selector

    configs = []

    # TODO: get more initial configs
    configs.append([image_point[0], image_point[1], np.pi, 30])
    configs.append([image_point[0], image_point[1], np.pi/2, 30])
    configs.append([image_point[0], image_point[1], np.pi/3, 30])
    configs.append([image_point[0], image_point[1], np.pi/4, 30])

    rectangle = get_rectangle_from_inference(rgbd, configs)

    if _DEBUG:
        rospy.loginfo('rectangle :\n{}'.format(rectangle))

    rectangles = [rectangle, 
                    gp.get_rect_from_config(configs[0]), 
                    gp.get_rect_from_config(configs[1]), 
                    gp.get_rect_from_config(configs[2]), 
                    gp.get_rect_from_config(configs[3])]

    write_rect = open('/home/hashb/tmp/plots/rectangles.txt', 'w')
    write_rect.write(str(rectangles))
    write_rect.close()

    # Convert image rectangle to workspace pose for gripper
    target_pose, obj_normal, obj_location = rectangle2pose(rectangles[0], normals_and_rgbd)
    init1_pose, init1_normal, init1_location = rectangle2pose(rectangles[1], normals_and_rgbd)
    init2_pose, init2_normal, init2_location = rectangle2pose(rectangles[2], normals_and_rgbd)
    init3_pose, init3_normal, init3_location = rectangle2pose(rectangles[3], normals_and_rgbd)
    init4_pose, init4_normal, init4_location = rectangle2pose(rectangles[4], normals_and_rgbd)


    # print 'Returning Response'
    response.gripper_pose = target_pose
    response.obj_normal = list(obj_normal)
    response.obj_location = obj_location
    response.init_1 = init1_pose
    response.init_2 = init2_pose
    response.init_3 = init3_pose
    response.init_4 = init4_pose

    return response


if __name__ == '__main__':
    rospy.init_node('grasp_inference_node')
    # rospy.Service('grasp_inf', GripperGraspInf, grasp_callback)
    rospy.Service('grasp_inf', GripperGraspInf, grasp_callback_table_segment)

    rospy.spin()
