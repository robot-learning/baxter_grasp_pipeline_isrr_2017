import tensorflow as tf
import numpy as np

#x = tf.Variable(2*np.ones((1, 2)), name='x')
x = tf.get_variable(dtype=tf.float64, name='x', shape=[
                    1, 2], initializer=tf.constant_initializer(2.))
x_holder = tf.placeholder(tf.float64, shape=[1, 2])
assign_x = x.assign(x_holder)
y = tf.Variable(3 * np.ones((1, 2)), name='y')
z = x * x + y * y
#z = x[0, 0]*x[0, 0]
# TypeError: Argument is not a tf.Variable
#z = x_holder*x_holder + y*y
#grads = opt.compute_gradients(z, var_list=[x_holder, y])

opt = tf.train.GradientDescentOptimizer(0.1)
grads = opt.compute_gradients(z, var_list=[x, y])
#grads = opt.compute_gradients(z[0, 0], var_list=[x])

init = tf.initialize_all_variables()
tf.get_default_graph().finalize()
sess = tf.Session()
sess.run(init)

sess.run([assign_x], feed_dict={x_holder: [[2, 3]]})
[grads_out, z_out] = sess.run([grads, z])
print grads_out, z_out
print grads_out[0]
print grads_out[0][0]
print grads_out[0][0][0]

sess.run([assign_x], feed_dict={x_holder: [[1, 2]]})
[grads_out, z_out] = sess.run([grads, z])
print grads_out, z_out
print grads_out[0]
print grads_out[0][0]
print grads_out[0][0][0]
