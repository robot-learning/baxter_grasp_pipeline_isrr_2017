#!/usr/bin/env python

import sys
import copy
import rospy
import roslib
import tf

import cv2
from cv_bridge import CvBridge, CvBridgeError

import numpy as np

import baxter_interface
import baxter_commander
import moveit_commander
from moveit_commander import RobotCommander, PlanningSceneInterface, roscpp_initialize, roscpp_shutdown
from baxter_commander import trajectories

from baxter_pipeline.srv import *
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from std_msgs.msg import Header
from sensor_msgs.msg import Image
from baxter_core_msgs.srv import *

class GraspExecutionNode(object):
    """A set of functions to move to an object and grasp it.
    We use ikservice to move to object, correcting our path
    using visual servoing. Goal is to move to the center of the
    object and grasp it along the approach vector."""

    def __init__(self):
        rospy.init_node('grasp_execution_ik')

        self.right = baxter_interface.Limb('right')
        self.left = baxter_interface.Limb('left')

        # this is to set the default arm
        self.arm = self.right

        self.right_gripper = baxter_interface.Gripper('right')
        self.left_gripper = baxter_interface.Gripper('left')

        self.gripper = self.right_gripper

        self.hand_camera_frame = 'right_hand_camera'
        self.hand_camera_topic = '/cameras/right_hand_camera/image'
        # self.open_camera("left", 640, 400)

        # baxter commander
        self.planner = baxter_commander.ArmCommander('right', fk='kdl', ik='trac')

        if self.left_gripper.error():
            self.left_gripper.reset()
        if self.right_gripper.error():
            self.right_gripper.reset()
        if (not self.left_gripper.calibrated() and
            self.left_gripper.type() != 'custom'):
            self.left_gripper.calibrate()
        if (not self.right_gripper.calibrated() and
            self.right_gripper.type() != 'custom'):
            self.right_gripper.calibrate()

        # camera parameters (NB. other parameters in open_camera)
        self.cam_calib    = 0.0025                     # meters per pixel at 1 meter
        self.cam_x_offset = 0.045                      # camera gripper offset 0.045
        self.cam_y_offset = -0.01                      # -0.01
        self.width        = 640                        # Camera resolution
        self.height       = 400

        # store endeffector pose
        self.pose = list(self.arm.endpoint_pose()['position'])

        self.tf = tf.TransformListener()
        self.grasp_inference_proxy = rospy.ServiceProxy('grasp_inf', GripperGraspInf)

        rospy.Subscriber(self.hand_camera_topic, Image, self.hand_camera_callback)
        # background image
        self.hand_camera_background_img = cv2.imread('/home/hashb/tmp/img/right.jpg')

        self.bridge = CvBridge()

        # Moveit
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()

        self.right_group = moveit_commander.MoveGroupCommander('right_arm')
        self.left_group = moveit_commander.MoveGroupCommander('left_arm')

        self.group = self.right_group

        self.table_pose = PoseStamped()  # self.get_posestamped_from_tuple(self.get_transform('/base', '/ar'))
        self.table_pose.pose.position.x =  1.21
        self.table_pose.pose.position.y =  0.03
        self.table_pose.pose.position.z -= 0.762/2

        camera_pose = self.get_transform('/base', '/camera_link')
        camera_pose = (camera_pose[0], camera_pose[1])

        self.camera_pose = self.get_posestamped_from_tuple(camera_pose)

        self.scene.add_box('table', self.table_pose, (1.524, 1.524, 0.762))
        self.scene.add_box('xtion', self.camera_pose, (0.0508, 0.1778, 0.0254))
        self.left_group.set_planner_id('RRTStarkConfigDefault')
        self.right_group.set_planner_id('RRTStarkConfigDefault')

        print ""
        print "Completed Initialization"

    def get_posestamped_from_tuple(self, pose_tuple):
        r = PoseStamped()
        r.header.stamp = rospy.Time()
        r.header.frame_id = 'base'
        r.pose = Pose(Point(*pose_tuple[0]), Quaternion(*pose_tuple[1]))
        return r

    def get_transform(self, from_frame, to_frame):
        try:
            self.tf.waitForTransform(from_frame, to_frame, rospy.Time.now(), rospy.Duration(4))
        except Exception as e:
            print e
            print "check if both cameras can see the marker"
            raise e
        if self.tf.frameExists(from_frame):
            if self.tf.frameExists(to_frame):
                latest_time = self.tf.getLatestCommonTime(from_frame, to_frame)
                position, quaternion = self.tf.lookupTransform(from_frame, to_frame, latest_time)
                return (position, quaternion)
            else:
                rospy.logerr("Cannot find {} frame".format(to_frame))
                raise ValueError('Frame is not present in the tf tree')
        else:
            rospy.logerr("Cannot find {} frame".format(from_frame))
            raise ValueError('Frame is not present in the tf tree')

    def hand_camera_callback(self, msg):
        try:
            self.hand_camera_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError, e:
            print e

    # convert Baxter point to image pixel
    def baxter_to_pixel(self, pt, dist):
        x = (self.width / 2)                                                         \
          + int((pt[1] - (self.pose[1] + self.cam_y_offset)) / (self.cam_calib * dist))
        y = (self.height / 2)                                                        \
          + int((pt[0] - (self.pose[0] + self.cam_x_offset)) / (self.cam_calib * dist))

        return (x, y)

    # convert image pixel to Baxter point
    def pixel_to_baxter(self, px, dist):
        x = ((px[1] - (self.height / 2)) * self.cam_calib * dist)                \
          + self.pose[0] + self.cam_x_offset
        y = ((px[0] - (self.width / 2)) * self.cam_calib * dist)                 \
          + self.pose[1] + self.cam_y_offset

        return (x, y)

    def get_camera_height_from_object(self):
        """we use the height to convert between pixel frame and world frame"""
        dist = abs(self.planner.endpoint_pose()[0][2] - self.pose[2])
        return dist

    def get_distance(self, limb):
        if limb == "left":
            dist = baxter_interface.analog_io.AnalogIO('left_hand_range').state()
        elif limb == "right":
            dist = baxter_interface.analog_io.AnalogIO('right_hand_range').state()
        else:
            sys.exit("ERROR - get_distance - Invalid limb")

        if dist > 65000:
            # sys.exit("ERROR - get_distance - no distance found")
            return self.get_camera_height_from_object()

        return float(dist / 1000.0)

    # Visual servoing : generate error
    def visual_servoing(self, img):
        """returns the difference between the location of
        object center and location of image center
        """
        # https://chaelatten.wordpress.com/2016/03/22/using-simpleblobdetector/
        # Setup SimpleBlobDetector parameters.
        # cv2.imwrite('/home/hashb/tmp/hand.jpg', img)
        # background = cv2.imread('/home/hashb/tmp/img/right.jpg')
        # img = img - background
        params = cv2.SimpleBlobDetector_Params()

        try:
            ver = (cv2.__version__).split('.')
            if int(ver[0]) < 3 :
                detector = cv2.SimpleBlobDetector(params)
            else :
                detector = cv2.SimpleBlobDetector_create(params)

            keypoints = detector.detect(img)
            sorted_keypoints = sorted(keypoints, key=lambda x: x.size)

            # lets select the largest keypoint
            largest_blob = sorted_keypoints[-1]
            # largest blob center
            blob_center = largest_blob.pt

        except Exception, e:
            print e
            img_size = img.shape
            blob_center = (img_size[0] / 2, img_size[1] / 2)
            # raise e

        # right hand coordinate system
        img_size = img.shape

        img_center = (img_size[0] / 2, img_size[1] / 2)

        # convert to base frame
        # http://sdk.rethinkrobotics.com/wiki/Worked_Example_Visual_Servoing
        height = self.get_distance("right")
        blob_center_in_base_frame = self.pixel_to_baxter(blob_center, height)
        img_center_in_base_frame = self.pixel_to_baxter(img_center, height)

        # difference from center
        img_diff_x = img_center_in_base_frame[0] - blob_center_in_base_frame[0]
        img_diff_y = img_center_in_base_frame[1] - blob_center_in_base_frame[1]

        return (img_diff_x, img_diff_y)

    def correct_error_using_visual_servoing(self):
        current_endeffector_pose = self.arm.endpoint_pose()

        # find the error
        error = self.visual_servoing(self.hand_camera_image)

        # add error to the pose
        position = [current_endeffector_pose['position'].x + error[0],
                    current_endeffector_pose['position'].y + error[1],
                    current_endeffector_pose['position'].z]
        orientation = list(current_endeffector_pose['orientation'])

        corrected_pose = self.list_to_pose([position, orientation])

        self.baxter_ik_move(corrected_pose, ik_only=True)


    def baxter_ik_move(self, pose, ik_only=False):
        """This is a stopping function. Tries to move baxter to pose
        and returns True
        """
        pose_stamped = pose
        if type(pose_stamped) is not PoseStamped:
            pose_stamped = self.list_to_pose(pose)
        if ik_only:
            self.planner.move_to_controlled(pose)
        else:
            # moveit
            self.group.set_start_state_to_current_state()
            self.group.set_pose_target(pose_stamped)
            plan = self.group.plan()
            self.group.execute(plan)

        rospy.sleep(1)
        self.pose = list(self.arm.endpoint_pose()['position'])
        # self.group.set_start_state_to_current_state()
        rospy.sleep(1)

    def pose_to_list(self, pose):
        if type(pose) == PoseStamped:
            pose = pose.pose
        position = [pose.position.x, pose.position.y, pose.position.z]
        quaternion = [pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w]
        return [position, quaternion]

    def list_to_pose(self, list_pose, frame='base'):
        position = list_pose[0]
        quaternion = list_pose[1]

        pose = PoseStamped()
        pose.header.frame_id = frame
        pose.header.stamp = rospy.Time.now()

        pose.pose.position.x = position[0]
        pose.pose.position.y = position[1]
        pose.pose.position.z = position[2]

        pose.pose.orientation.x = quaternion[0]
        pose.pose.orientation.y = quaternion[1]
        pose.pose.orientation.z = quaternion[2]
        pose.pose.orientation.w = quaternion[3]

        return pose

    def get_inference(self):
        """ We get the inference grasp pose and transform it to base frame
        and return the transformed frame for now.
        """
        grasp_inference_response = self.grasp_inference_proxy()
        gripper_poses = [grasp_inference_response.gripper_pose,
                         grasp_inference_response.init_1,
                         grasp_inference_response.init_2,
                         grasp_inference_response.init_3,
                         grasp_inference_response.init_4]

        obj_pose = self.tf.transformPose('/base', grasp_inference_response.obj_location)
        return gripper_poses, obj_pose

    def broadcast_goal_location(self, pose):
        print "Broadcasting goal location"
        print "Ctrl+C to stop doesnt work"
        br = tf.TransformBroadcaster()

        for x_ in xrange(1,20):
            br.sendTransform((pose.pose.position.x, pose.pose.position.y,
                              pose.pose.position.z),
                             (pose.pose.orientation.x, pose.pose.orientation.y,
                             pose.pose.orientation.z, pose.pose.orientation.w),
                             rospy.Time.now(),
                             'gripper_pose',
                             pose.header.frame_id)
            rospy.sleep(1)
        # exit(0)



    def run(self, grasp_pose, obj_pose):
        """
        1. get the inference from get_inference
        2. use baxter_ik_move to move to the first location
        3. run visual servoing until error is very low
        4. move forward
        5. run visual servoing until error is very low
        6. move forward to object
        7. Attempt to grasp the object
        8. move object up by 40cms
        9. wait for 10 seconds
        10. move to a different location and place the object
        """

        self.initial_pose, self.obj_pose = grasp_pose, obj_pose  # self.get_inference()
        obj_z = self.obj_pose.pose.position.z  - 0.02

        self.broadcast_goal_location(self.initial_pose)

        if raw_input("should I execute? [Y/n] ") == "n":
            return
            # exit(0)

        self.baxter_ik_move(self.initial_pose)

        # First move to half the distance to the object
        # self.correct_error_using_visual_servoing()
        final_pose = self.planner.endpoint_pose()
        final_pose[0][2] = obj_z/2
        self.baxter_ik_move(final_pose)

        # correct position again and move to final goal
        # self.correct_error_using_visual_servoing()
        final_pose = self.planner.endpoint_pose()
        final_pose[0][2] = obj_z
        self.baxter_ik_move(final_pose)

        rospy.sleep(2)

        # Grasp the object
        self.gripper.close()

        rospy.sleep(2)

        # move straight up by 30 cms
        intermidiate_pose = self.planner.endpoint_pose()
        intermidiate_pose[0][2] += 0.20

        self.baxter_ik_move(intermidiate_pose, ik_only=True)

        rospy.sleep(30)

        # move to the right
        intermidiate_pose = self.planner.endpoint_pose()
        intermidiate_pose[0][1] -= 0.35
        # intermidiate_pose[0][2] -= 0.20

        self.baxter_ik_move(intermidiate_pose, ik_only=True)

        intermidiate_pose = self.planner.endpoint_pose()
        # intermidiate_pose[0][1] -= 0.35
        intermidiate_pose[0][2] -= 0.20

        self.baxter_ik_move(intermidiate_pose, ik_only=True)


        self.gripper.open()


# if __name__ == '__main__':
#     n = GraspExecutionNode()
#     n.run()
