#!/usr/bin/env python

"""
Automate baxter to kinect/xtion pose calibration

"""

# We will first launch aruco_ros baxter_kinect.launch
# then we try to get the transform 


import rospy, rospkg
from tf import TransformListener
import numpy as np
from numpy.linalg import eig

BAXTER_STATIC = """
<launch>
    <node pkg="tf" type="static_transform_publisher" name="baxter_ar_link" args="{} /torso /ar 100" />
    <node pkg="tf" type="static_transform_publisher" name="kinect_ar_link" args="{} /ar /camera_link 100" />
</launch>"""

class CalibrationNode:
    def __init__(self, baxter_torso_frame = "torso", 
                baxter_marker_frame = "ar_baxter", 
                kinect_marker_frame = "ar_kinect", 
                kinect_camera_frame = "camera_link"):
        
        self.baxter_torso_frame = "torso"
        self.baxter_marker_frame = "ar_baxter"
        self.kinect_marker_frame = "ar_kinect"
        self.kinect_camera_frame = "camera_link"
        
        rospy.init_node("calibration_node")
        self.tf = TransformListener()
        rospy.sleep(20)

        self.baxter_pipeline_path = rospkg.RosPack().get_path('baxter_pipeline')


    def get_transform(self, from_frame, to_frame):
        try:
            self.tf.waitForTransform(from_frame, to_frame, rospy.Time.now(), rospy.Duration(4))
        except Exception as e:
            print e
            print "check if both cameras can see the marker"
            raise e
        if self.tf.frameExists(from_frame):
            if self.tf.frameExists(to_frame):
                latest_time = self.tf.getLatestCommonTime(from_frame, to_frame)
                position, quaternion = self.tf.lookupTransform(from_frame, to_frame, latest_time)
                # print position, quaternion
                return (position, quaternion)  # (x,y,z), (x,y,z,w)
            else:
                rospy.logerr("Cannot find {} frame".format(to_frame))
                raise ValueError('check if your cameras are looking at the marker and your robot tf is loaded') 
        else:
            rospy.logerr("Cannot find {} frame".format(from_frame))
            raise ValueError('check if your cameras are looking at the marker and your robot tf is loaded') 

    def get_mean_pose(self, from_frame, to_frame, num_itr):
        position_arr = []
        quaternion_arr = []

        for x in range(num_itr):
            print "Sample number: ", x
            position, quaternion = self.get_transform(from_frame, to_frame)
            position_arr.append(position)
            quaternion_arr.append(quaternion)

        position_arr = np.array(position_arr)
        quaternion_arr = np.array(quaternion_arr)

        position_mean = np.mean(position_arr, axis=0)
        quaternion_mean = self.get_quaternion_average(quaternion_arr)

        return position_mean, quaternion_mean

    def get_quaternion_average(self, quaternions):
        # FIXME
        return quaternions[0,:]

        # http://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
        # http://stackoverflow.com/a/29315869

        # Form the symmetric accumulator matrix
        A = np.zeros((4, 4))
        M = len(quaternions)

        for i in range(M):
            q = quaternions[i,:]
            A = (q*q.T) + A

        quaternion_avg = eig(A)
        return quaternion_avg


    def run(self):
        # First we will get baxter transform to ar
        print "Fetching baxter_ar pose"
        baxter_ar_position, baxter_ar_quaternion = self.get_mean_pose(self.baxter_torso_frame, self.baxter_marker_frame, 100)
        print "Got baxter_ar pose"
        print "Fetching kinect_ar pose"
        kinect_ar_position, kinect_ar_quaternion = self.get_mean_pose(self.kinect_marker_frame, self.kinect_camera_frame, 100)
        print "Got kinect_ar pose"
        # print baxter_ar_position
        # print baxter_ar_quaternion

        # x y z w x y z
        baxter_ar_tf = "{} {} {} {} {} {} {}".format(baxter_ar_position[0], 
                                                    baxter_ar_position[1],
                                                    baxter_ar_position[2],
                                                    baxter_ar_quaternion[0], 
                                                    baxter_ar_quaternion[1], 
                                                    baxter_ar_quaternion[2], 
                                                    baxter_ar_quaternion[3])
        kinect_ar_tf = "{} {} {} {} {} {} {}".format(kinect_ar_position[0],
                                                    kinect_ar_position[1],
                                                    kinect_ar_position[2],
                                                    kinect_ar_quaternion[0],
                                                    kinect_ar_quaternion[1],
                                                    kinect_ar_quaternion[2],
                                                    kinect_ar_quaternion[3],)

        
        baxter_static_file = open(self.baxter_pipeline_path + "/launch/baxter_static.launch", 'w')
        baxter_static_file.write(BAXTER_STATIC.format(baxter_ar_tf, kinect_ar_tf))
        baxter_static_file.close()
        print "Done."


if __name__ == '__main__':

    calibration = CalibrationNode(baxter_torso_frame = "torso", 
                                    baxter_marker_frame = "ar_baxter", 
                                    kinect_marker_frame = "ar_kinect", 
                                    kinect_camera_frame = "camera_link")
    calibration.run()
    print "We are done here. Press Ctrl+C to exit"

