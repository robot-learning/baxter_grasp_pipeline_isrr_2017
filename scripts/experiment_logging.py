#!/usr/bin/env python

"""
Log experiment data

ask which file to save to: file name
ask number of trials each

ask object name
ask if success or fail
ask for comments
"""
import os
import sys
# import rospy

import visual_servoing

class ExperimentLogWriter(object):
    """docstring for ExperimentLogWriter"""
    def __init__(self, path):
        self.grasp_exec = visual_servoing.GraspExecutionNode()

        self.logfile = path
        self.grasp_names = ["inference", "init_1", "init_2", "init_3", "init_4"]
        self.object_name = raw_input("object name: ")
        # self.num_trials = int(raw_input("Number of trials for each object: "))

    def log_writer(self, object_name, grasp, success, comment):
        log_file = open(self.logfile + object_name + ".log", 'a')
        data = "{}, {}, {}, {} \n".format(object_name, grasp, success, comment)
        log_file.write(data)
        log_file.close()

    def run(self):
        for x in ["1", "2", "3"]:
            
            gripper_poses, object_pose = self.grasp_exec.get_inference()
            for idx, pose in enumerate(gripper_poses):
                grasp = self.grasp_names[idx]
                self.grasp_exec.run(pose, object_pose)
                success = raw_input("Grasp successful [y/n]: ")
                comment = raw_input("additional comments: ")
                self.log_writer(self.object_name + "_" + x, grasp, success, comment)



if __name__ == '__main__':
    log = ExperimentLogWriter('/home/hashb/tmp/log/')
    log.run()
