Baxter Grasping Pipeline
---------------

First run the `calibration.launch` which write `baxter_static.launch` to the launch directory. This calibrates kinect with baxter.

```
roslaunch baxter_pipeline calibration.launch
```

Then open terminator and divide it into 6 terminals. For each terminal, type these commands in the same order

```bash
# Controller for baxter
rosrun baxter_interface joint_trajectory_action_server.py

# Launches openni for xtion camera, moveit and rviz
roslaunch baxter_pipeline baxter_xtion.launch

# Launches table segmentation code
roslaunch baxter_pipeline table_segmenter.launch

# Launches Grasp inference service
roslaunch baxter_pipeline grasp_srv.launch

# runs gripper cuff control example. We use this to open and close the gripper
# for debugging
rosrun baxter_examples gripper_cuff_control.py

# Runs the script to control moveit and execute grasps
rosrun baxter_pipeline baxter_moveit.py

```

## Experimental method

- Use pointcloud indices instead of camera calibration to get the pose
- Use visual servoing to move to pose
